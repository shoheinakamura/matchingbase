class AddColumnUploadFileToChatMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_messages, :upload_file, :string, after: :content  unless ChatMessage.column_names.include?('upload_file')
    add_index :chat_messages, :upload_file unless index_exists?(:chat_messages, :upload_file)
  end
end
