class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('posts')
    create_table :posts do |t|
      t.integer :user_id
      t.string :title
      t.string :main_image
      t.integer :category_id
      t.text :content, limit: 4294967295
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :posts, :user_id
    add_index :posts, :title
    add_index :posts, :main_image
    add_index :posts, :category_id
    add_index :posts, :deleted_at
    add_index :posts, :created_at
    add_index :posts, :updated_at
  end
end
