class CreateChatMessages < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('chat_messages')
    create_table :chat_messages do |t|
      t.integer  :chat_room_id
      t.integer  :user_id
      t.text     :content
      t.timestamps
    end
    add_index :chat_messages, :chat_room_id
    add_index :chat_messages, :user_id
    add_index :chat_messages, :created_at
    add_index :chat_messages, :updated_at
  end
end
