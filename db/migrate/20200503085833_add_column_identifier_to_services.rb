class AddColumnIdentifierToServices < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :identifier, :string, after: :id unless Service.column_names.include?('identifier')
    add_index :services, :identifier, unique: true unless index_exists?(:services, :identifier)
  end
end
