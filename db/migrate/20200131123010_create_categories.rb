class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('categories')
    create_table :categories do |t|
      t.string :name
      t.integer :service_id
      t.timestamps
    end
    add_index :categories, :name
    add_index :categories, :service_id
    add_index :categories, :created_at
    add_index :categories, :updated_at
  end
end
