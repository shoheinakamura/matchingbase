class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('contacts')
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.text :message
      t.timestamps
    end
    add_index :contacts, :name
    add_index :contacts, :email
    add_index :contacts, :created_at
    add_index :contacts, :updated_at
  end
end
