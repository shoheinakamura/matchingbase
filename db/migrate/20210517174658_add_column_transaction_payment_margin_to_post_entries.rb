class AddColumnTransactionPaymentMarginToPostEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :post_entries, :transaction_payment_margin, :float, after: :deposited_price  unless PostEntry.column_names.include?('transaction_payment_margin')
    add_index :post_entries, :transaction_payment_margin unless index_exists?(:post_entries, :transaction_payment_margin)
  end
end
