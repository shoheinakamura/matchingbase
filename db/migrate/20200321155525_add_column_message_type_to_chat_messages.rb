class AddColumnMessageTypeToChatMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_messages, :message_type, :integer, after: :content  unless ChatMessage.column_names.include?('message_type')
    add_index :chat_messages, :message_type unless index_exists?(:chat_messages, :message_type)
  end
end
