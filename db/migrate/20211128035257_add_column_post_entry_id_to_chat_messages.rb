class AddColumnPostEntryIdToChatMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_messages, :post_entry_id, :bigint, after: :upload_file unless ChatMessage.column_names.include?('post_entry_id')
    add_index :chat_messages, :post_entry_id unless index_exists?(:chat_messages, :post_entry_id)
  end
end
