class CreatePostTags < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('post_tags')
    create_table :post_tags do |t|
      t.integer :post_id
      t.integer :tag_id
      t.timestamps
    end
    add_index :post_tags, :post_id
    add_index :post_tags, :tag_id
    add_index :post_tags, :created_at
    add_index :post_tags, :updated_at
  end
end
