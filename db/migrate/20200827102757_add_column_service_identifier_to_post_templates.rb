class AddColumnServiceIdentifierToPostTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :post_templates, :service_identifier, :string, after: :content  unless PostTemplate.column_names.include?('service_identifier')
    add_index :post_templates, :service_identifier unless index_exists?(:post_templates, :service_identifier)
  end
end
