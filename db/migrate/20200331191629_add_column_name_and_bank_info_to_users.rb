class AddColumnNameAndBankInfoToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string, after: :email  unless User.column_names.include?('name')

    # bank_name
    add_column :users, :bank_name, :string, after: :pay_jp_fingerprint  unless User.column_names.include?('bank_name')
    add_column :users, :encrypted_bank_name, :string, after: :bank_name  unless User.column_names.include?('encrypted_bank_name')
    add_column :users, :encrypted_bank_name_iv, :string, after: :encrypted_bank_name  unless User.column_names.include?('encrypted_bank_name_iv')
    # bank_code
    add_column :users, :bank_code, :string, after: :encrypted_bank_name_iv  unless User.column_names.include?('bank_code')
    add_column :users, :encrypted_bank_code, :string, after: :bank_code  unless User.column_names.include?('encrypted_bank_code')
    add_column :users, :encrypted_bank_code_iv, :string, after: :encrypted_bank_code  unless User.column_names.include?('encrypted_bank_code_iv')
    # bank_branch_name
    add_column :users, :bank_branch_name, :string, after: :encrypted_bank_code_iv  unless User.column_names.include?('bank_branch_name')
    add_column :users, :encrypted_bank_branch_name, :string, after: :bank_branch_name  unless User.column_names.include?('encrypted_bank_branch_name')
    add_column :users, :encrypted_bank_branch_name_iv, :string, after: :encrypted_bank_branch_name  unless User.column_names.include?('encrypted_bank_branch_name_iv')
    # bank_branch_number
    add_column :users, :bank_branch_number, :string, after: :encrypted_bank_branch_name_iv  unless User.column_names.include?('bank_branch_number')
    add_column :users, :encrypted_bank_branch_number, :string, after: :bank_branch_number  unless User.column_names.include?('encrypted_bank_branch_number')
    add_column :users, :encrypted_bank_branch_number_iv, :string, after: :encrypted_bank_branch_number  unless User.column_names.include?('encrypted_bank_branch_number_iv')
    # bank_account_type
    add_column :users, :bank_account_type, :string, after: :encrypted_bank_branch_number_iv  unless User.column_names.include?('bank_account_type')
    add_column :users, :encrypted_bank_account_type, :string, after: :bank_account_type  unless User.column_names.include?('encrypted_bank_account_type')
    add_column :users, :encrypted_bank_account_type_iv, :string, after: :encrypted_bank_account_type  unless User.column_names.include?('encrypted_bank_account_type_iv')
    # bank_account_number
    add_column :users, :bank_account_number, :string, after: :encrypted_bank_account_type_iv  unless User.column_names.include?('bank_account_number')
    add_column :users, :encrypted_bank_account_number, :string, after: :bank_account_number  unless User.column_names.include?('encrypted_bank_account_number')
    add_column :users, :encrypted_bank_account_number_iv, :string, after: :encrypted_bank_account_number  unless User.column_names.include?('encrypted_bank_account_number_iv')
    # bank_account_holder
    add_column :users, :bank_account_holder, :string, after: :encrypted_bank_account_number_iv  unless User.column_names.include?('bank_account_holder')
    add_column :users, :encrypted_bank_account_holder, :string, after: :bank_account_holder  unless User.column_names.include?('encrypted_bank_account_holder')
    add_column :users, :encrypted_bank_account_holder_iv, :string, after: :encrypted_bank_account_holder  unless User.column_names.include?('encrypted_bank_account_holder_iv')

    # ------ add_index ------
    add_index :users, :name unless index_exists?(:users, :name)
    # bank_name
    add_index :users, :bank_name unless index_exists?(:users, :bank_name)
    add_index :users, :encrypted_bank_name unless index_exists?(:users, :encrypted_bank_name)
    add_index :users, :encrypted_bank_name_iv unless index_exists?(:users, :encrypted_bank_name_iv)
    # bank_code
    add_index :users, :bank_code unless index_exists?(:users, :bank_code)
    add_index :users, :encrypted_bank_code unless index_exists?(:users, :encrypted_bank_code)
    add_index :users, :encrypted_bank_code_iv unless index_exists?(:users, :encrypted_bank_code_iv)
    # bank_branch_name
    add_index :users, :bank_branch_name unless index_exists?(:users, :bank_branch_name)
    add_index :users, :encrypted_bank_branch_name unless index_exists?(:users, :encrypted_bank_branch_name)
    add_index :users, :encrypted_bank_branch_name_iv unless index_exists?(:users, :encrypted_bank_branch_name_iv)
    # bank_branch_number
    add_index :users, :bank_branch_number unless index_exists?(:users, :bank_branch_number)
    add_index :users, :encrypted_bank_branch_number unless index_exists?(:users, :encrypted_bank_branch_number)
    add_index :users, :encrypted_bank_branch_number_iv unless index_exists?(:users, :encrypted_bank_branch_number_iv)
    # bank_account_type
    add_index :users, :bank_account_type unless index_exists?(:users, :bank_account_type)
    add_index :users, :encrypted_bank_account_type unless index_exists?(:users, :encrypted_bank_account_type)
    add_index :users, :encrypted_bank_account_type_iv unless index_exists?(:users, :encrypted_bank_account_type_iv)
    # bank_account_number
    add_index :users, :bank_account_number unless index_exists?(:users, :bank_account_number)
    add_index :users, :encrypted_bank_account_number unless index_exists?(:users, :encrypted_bank_account_number)
    add_index :users, :encrypted_bank_account_number_iv unless index_exists?(:users, :encrypted_bank_account_number_iv)
    # bank_account_holder
    add_index :users, :bank_account_holder unless index_exists?(:users, :bank_account_holder)
    add_index :users, :encrypted_bank_account_holder unless index_exists?(:users, :encrypted_bank_account_holder)
    add_index :users, :encrypted_bank_account_holder_iv unless index_exists?(:users, :encrypted_bank_account_holder_iv)
  end
end
