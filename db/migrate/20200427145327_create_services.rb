class CreateServices < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('services')
    create_table :services do |t|
      t.integer :provider_id
      t.string :name
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :services, :provider_id
    add_index :services, :name
    add_index :services, :deleted_at
    add_index :services, :created_at
    add_index :services, :updated_at
  end
end
