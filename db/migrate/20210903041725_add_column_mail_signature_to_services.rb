class AddColumnMailSignatureToServices < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :mail_signature, :text, after: :contact_form_mail unless Service.column_names.include?('mail_signature')
  end
end
