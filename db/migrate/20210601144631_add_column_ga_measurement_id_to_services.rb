class AddColumnGaMeasurementIdToServices < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :ga_measurement_id, :string, limit: 20, after: :main_color  unless Service.column_names.include?('ga_measurement_id')
    add_index :services, :ga_measurement_id unless index_exists?(:services, :ga_measurement_id)
  end
end
