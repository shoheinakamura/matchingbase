class AddColumnDepositedPriceToPostEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :post_entries, :deposited_price, :integer, after: :price  unless PostEntry.column_names.include?('deposited_price')
    add_index :post_entries, :deposited_price unless index_exists?(:post_entries, :deposited_price)
  end
end
