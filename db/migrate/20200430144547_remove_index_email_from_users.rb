class RemoveIndexEmailFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_index :users, column: :email, unique: true if index_exists?(:users, :email)
    add_index :users, [:service_identifier, :email], unique: true unless index_exists?(:users, [:service_identifier, :email])
  end
end
