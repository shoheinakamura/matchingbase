class AddColumnTrialExpiredAtToProviders < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :trial_expired_at, :datetime, after: :campaign_plan_id unless Provider.column_names.include?('trial_expired_at')
    add_index :providers, :trial_expired_at unless index_exists?(:providers, :trial_expired_at)
  end
end
