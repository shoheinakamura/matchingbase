class AddColumnServicesIdentifierToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :service_identifier, :string, after: :id unless User.column_names.include?('service_identifier')
    add_index :users, :service_identifier unless index_exists?(:users, :service_identifier)
  end
end
