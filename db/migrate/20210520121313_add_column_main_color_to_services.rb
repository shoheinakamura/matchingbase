class AddColumnMainColorToServices < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :main_color, :string, limit: 20, after: :favicon_image  unless Service.column_names.include?('main_color')
    add_index :services, :main_color unless index_exists?(:services, :main_color)
  end
end
