class AddColumnPayJpSubscriptionIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :pay_jp_subscription_id, :string, after: :pay_jp_customer_id unless User.column_names.include?('pay_jp_subscription_id')

    add_index :users, :pay_jp_subscription_id unless index_exists?(:users, :pay_jp_subscription_id)
  end
end
