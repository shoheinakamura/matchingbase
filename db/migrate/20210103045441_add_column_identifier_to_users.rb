class AddColumnIdentifierToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :identifier, :string, after: :id unless User.column_names.include?('identifier')
    add_index :users, :identifier, unique: true unless index_exists?(:users, :identifier)
  end
end
