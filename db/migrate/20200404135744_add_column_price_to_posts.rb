class AddColumnPriceToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :price, :integer, after: :content  unless Post.column_names.include?('price')
    add_index :posts, :price unless index_exists?(:posts, :price)
  end
end
