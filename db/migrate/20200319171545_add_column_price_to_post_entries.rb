class AddColumnPriceToPostEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :post_entries, :price, :integer, after: :status  unless PostEntry.column_names.include?('price')
    add_index :post_entries, :price unless index_exists?(:post_entries, :price)
  end
end
