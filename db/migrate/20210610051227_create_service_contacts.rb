class CreateServiceContacts < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('service_contacts')
    create_table :service_contacts do |t|
      t.string :service_identifier, null:false
      t.bigint :user_id
      t.string :name
      t.string :email
      t.text :message
      t.timestamps
    end
    add_index :service_contacts, :service_identifier
    add_index :service_contacts, :user_id
    add_index :service_contacts, :name
    add_index :service_contacts, :email
    add_index :service_contacts, :created_at
    add_index :service_contacts, :updated_at
  end
end
