class AddColumnLastReadAtToChatMembers < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_members, :last_read_at, :datetime, after: :user_id  unless ChatMember.column_names.include?('last_read_at')
    add_index :chat_members, :last_read_at unless index_exists?(:chat_members, :last_read_at)
  end
end
