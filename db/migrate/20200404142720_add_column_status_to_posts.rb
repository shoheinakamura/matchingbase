class AddColumnStatusToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :status, :integer, after: :content, default: 1 unless Post.column_names.include?('status')
    add_index :posts, :status unless index_exists?(:posts, :status)
  end
end
