class AddColumnPayJpToProviders < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :pay_jp_customer_id, :string, after: :unconfirmed_email  unless Provider.column_names.include?('pay_jp_customer_id')
    add_column :providers, :pay_jp_subscription_id, :string, after: :pay_jp_customer_id unless Provider.column_names.include?('pay_jp_subscription_id')
    add_column :providers, :pay_jp_fingerprint, :string, after: :pay_jp_subscription_id unless Provider.column_names.include?('pay_jp_fingerprint')
    add_column :providers, :plan_id, :string, after: :pay_jp_fingerprint unless Provider.column_names.include?('plan_id')
    add_column :providers, :campaign_plan_id, :string, after: :plan_id unless Provider.column_names.include?('campaign_plan_id')

    add_index :providers, :pay_jp_customer_id unless index_exists?(:providers, :pay_jp_customer_id)
    add_index :providers, :pay_jp_fingerprint unless index_exists?(:providers, :pay_jp_fingerprint)
    add_index :providers, :pay_jp_subscription_id unless index_exists?(:providers, :pay_jp_subscription_id)
    add_index :providers, :plan_id unless index_exists?(:providers, :plan_id)
    add_index :providers, :campaign_plan_id unless index_exists?(:providers, :campaign_plan_id)
  end
end
