class AddColumnUnsubscribeAtToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :unsubscribe_at, :datetime, after: :unsubscribe_reason unless User.column_names.include?('unsubscribe_at')
    add_index :users, :unsubscribe_at unless index_exists?(:users, :unsubscribe_at)
  end
end
