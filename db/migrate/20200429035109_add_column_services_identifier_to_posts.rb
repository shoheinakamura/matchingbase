class AddColumnServicesIdentifierToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :service_identifier, :string, after: :user_id unless Post.column_names.include?('service_identifier')
    add_index :posts, :service_identifier unless index_exists?(:posts, :service_identifier)
  end
end
