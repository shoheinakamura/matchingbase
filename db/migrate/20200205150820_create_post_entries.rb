class CreatePostEntries < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('post_entries')
    create_table :post_entries do |t|
      t.integer :post_id
      t.integer :user_id
      t.integer :status
      t.datetime :entry_at
      t.timestamps
    end
    add_index :post_entries, :post_id
    add_index :post_entries, :user_id
    add_index :post_entries, :status
    add_index :post_entries, :entry_at
    add_index :post_entries, :created_at
    add_index :post_entries, :updated_at
  end
end
