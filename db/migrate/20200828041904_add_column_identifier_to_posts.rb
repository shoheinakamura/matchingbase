class AddColumnIdentifierToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :identifier, :string, after: :id unless Post.column_names.include?('identifier')
    add_index :posts, :identifier, unique: true unless index_exists?(:posts, :identifier)
  end
end
