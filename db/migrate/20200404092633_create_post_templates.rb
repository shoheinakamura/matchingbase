class CreatePostTemplates < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('post_templates')
    create_table :post_templates do |t|
      t.text :content, limit: 4294967295
      t.timestamps
    end
    add_index :post_templates, :created_at
    add_index :post_templates, :updated_at
  end
end
