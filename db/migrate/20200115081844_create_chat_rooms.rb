class CreateChatRooms < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('chat_rooms')
    create_table :chat_rooms do |t|
      t.string :service_identifier
      t.string :identifier
      t.timestamps
    end
    add_index :chat_rooms, :service_identifier
    add_index :chat_rooms, :identifier
    add_index :chat_rooms, :created_at
    add_index :chat_rooms, :updated_at
  end
end
