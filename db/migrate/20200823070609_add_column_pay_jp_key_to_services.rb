class AddColumnPayJpKeyToServices < ActiveRecord::Migration[6.0]
  def change
    add_column :services, :pay_jp_public_key, :string, after: :subscription_payment_limit_type unless Service.column_names.include?('pay_jp_public_key')
    add_column :services, :pay_jp_private_key, :string, after: :pay_jp_public_key unless Service.column_names.include?('pay_jp_private_key')
    add_column :services, :pay_jp_plan_id, :string, after: :pay_jp_private_key unless Service.column_names.include?('pay_jp_plan_id')

    add_index :services, :pay_jp_public_key unless index_exists?(:services, :pay_jp_public_key)
    add_index :services, :pay_jp_private_key unless index_exists?(:services, :pay_jp_private_key)
    add_index :services, :pay_jp_plan_id unless index_exists?(:services, :pay_jp_plan_id)
  end
end
