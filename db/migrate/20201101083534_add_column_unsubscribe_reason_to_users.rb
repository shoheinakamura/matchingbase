class AddColumnUnsubscribeReasonToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :unsubscribe_reason, :text, after: :encrypted_bank_account_holder_iv unless User.column_names.include?('unsubscribe_reason')
  end
end
