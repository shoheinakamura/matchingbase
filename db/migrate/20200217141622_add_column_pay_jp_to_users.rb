class AddColumnPayJpToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :pay_jp_customer_id, :string, after: :unconfirmed_email  unless User.column_names.include?('pay_jp_customer_id')
    add_column :users, :pay_jp_fingerprint, :string, after: :pay_jp_customer_id unless User.column_names.include?('pay_jp_fingerprint')
    add_index :users, :pay_jp_customer_id unless index_exists?(:users, :pay_jp_customer_id)
    add_index :users, :pay_jp_fingerprint unless index_exists?(:users, :pay_jp_fingerprint)
  end
end
