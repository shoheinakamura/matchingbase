class AddColumnProfileImageToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :profile_image, :string, after: :name  unless User.column_names.include?('profile_image')
    add_index :users, :profile_image unless index_exists?(:users, :profile_image)
  end
end
