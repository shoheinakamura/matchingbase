class AddColumnUnsubscribeReasonToProviders < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :unsubscribe_reason, :text, after: :campaign_plan_id unless Provider.column_names.include?('unsubscribe_reason')
  end
end
