class CreateTags < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('tags')
    create_table :tags do |t|
      t.string :name
      t.integer :service_id
      t.timestamps
    end
    add_index :tags, :name
    add_index :tags, :service_id
    add_index :tags, :created_at
    add_index :tags, :updated_at
  end
end
