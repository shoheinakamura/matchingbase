class AddColumnDeletedAtToChatMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :chat_messages, :deleted_at, :datetime, after: :message_type unless ChatMessage.column_names.include?('deleted_at')
    add_index :chat_messages, :deleted_at unless index_exists?(:chat_messages, :deleted_at)
  end
end
