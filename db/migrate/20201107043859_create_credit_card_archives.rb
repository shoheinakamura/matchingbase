class CreateCreditCardArchives < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('credit_card_archives')
    create_table :credit_card_archives do |t|
      t.integer :provider_id
      t.string :pay_jp_fingerprint
      t.integer :reason
      t.timestamps
    end
    add_index :credit_card_archives, :provider_id
    add_index :credit_card_archives, :pay_jp_fingerprint
    add_index :credit_card_archives, :reason
    add_index :credit_card_archives, :created_at
    add_index :credit_card_archives, :updated_at
  end
end
