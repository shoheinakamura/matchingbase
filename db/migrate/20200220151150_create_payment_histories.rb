class CreatePaymentHistories < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('payment_histories')
    create_table :payment_histories do |t|
      t.integer :post_entry_id
      t.integer :user_id
      t.string :pay_jp_charge_id
      t.timestamps
    end
    add_index :payment_histories, :post_entry_id
    add_index :payment_histories, :user_id
    add_index :payment_histories, :pay_jp_charge_id
    add_index :payment_histories, :created_at
    add_index :payment_histories, :updated_at
  end
end
