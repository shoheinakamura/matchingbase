class CreateChatMembers < ActiveRecord::Migration[6.0]
  def change
    return if table_exists?('chat_members')
    create_table :chat_members do |t|
      t.integer  :chat_room_id
      t.integer  :user_id
      t.timestamps
    end
    add_index :chat_members, :chat_room_id
    add_index :chat_members, :user_id
    add_index :chat_members, :created_at
    add_index :chat_members, :updated_at
  end
end
