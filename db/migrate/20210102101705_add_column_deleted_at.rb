class AddColumnDeletedAt < ActiveRecord::Migration[6.0]
  def change
    add_column :categories, :deleted_at, :datetime, after: :service_id unless Category.column_names.include?('deleted_at')
    add_column :chat_members, :deleted_at, :datetime, after: :last_read_at unless ChatMember.column_names.include?('deleted_at')
    add_column :chat_rooms, :deleted_at, :datetime, after: :identifier unless ChatRoom.column_names.include?('deleted_at')
    add_column :payment_histories, :deleted_at, :datetime, after: :pay_jp_charge_id unless PaymentHistory.column_names.include?('deleted_at')
    add_column :post_entries, :deleted_at, :datetime, after: :entry_at unless PostEntry.column_names.include?('deleted_at')
    add_column :post_tags, :deleted_at, :datetime, after: :tag_id unless PostTag.column_names.include?('deleted_at')
    add_column :post_templates, :deleted_at, :datetime, after: :service_identifier unless PostTemplate.column_names.include?('deleted_at')
    add_column :tags, :deleted_at, :datetime, after: :service_id unless Tag.column_names.include?('deleted_at')

    add_index :categories, :deleted_at unless index_exists?(:categories, :deleted_at)
    add_index :chat_members, :deleted_at unless index_exists?(:chat_members, :deleted_at)
    add_index :chat_rooms, :deleted_at unless index_exists?(:chat_rooms, :deleted_at)
    add_index :payment_histories, :deleted_at unless index_exists?(:payment_histories, :deleted_at)
    add_index :post_entries, :deleted_at unless index_exists?(:post_entries, :deleted_at)
    add_index :post_tags, :deleted_at unless index_exists?(:post_tags, :deleted_at)
    add_index :post_templates, :deleted_at unless index_exists?(:post_templates, :deleted_at)
    add_index :tags, :deleted_at unless index_exists?(:tags, :deleted_at)
  end
end
