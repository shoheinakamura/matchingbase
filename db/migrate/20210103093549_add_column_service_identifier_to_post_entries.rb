class AddColumnServiceIdentifierToPostEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :post_entries, :service_identifier, :string, after: :user_id  unless PostEntry.column_names.include?('service_identifier')
    add_index :post_entries, :service_identifier unless index_exists?(:post_entries, :service_identifier)
  end
end
