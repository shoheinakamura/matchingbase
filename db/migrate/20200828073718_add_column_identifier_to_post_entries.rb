class AddColumnIdentifierToPostEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :post_entries, :identifier, :string, after: :id unless PostEntry.column_names.include?('identifier')
    add_index :post_entries, :identifier, unique: true unless index_exists?(:post_entries, :identifier)
  end
end
