# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_28_035257) do

  create_table "categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.integer "service_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_categories_on_created_at"
    t.index ["deleted_at"], name: "index_categories_on_deleted_at"
    t.index ["name"], name: "index_categories_on_name"
    t.index ["service_id"], name: "index_categories_on_service_id"
    t.index ["updated_at"], name: "index_categories_on_updated_at"
  end

  create_table "chat_members", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "chat_room_id"
    t.integer "user_id"
    t.datetime "last_read_at"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["chat_room_id"], name: "index_chat_members_on_chat_room_id"
    t.index ["created_at"], name: "index_chat_members_on_created_at"
    t.index ["deleted_at"], name: "index_chat_members_on_deleted_at"
    t.index ["last_read_at"], name: "index_chat_members_on_last_read_at"
    t.index ["updated_at"], name: "index_chat_members_on_updated_at"
    t.index ["user_id"], name: "index_chat_members_on_user_id"
  end

  create_table "chat_messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "chat_room_id"
    t.integer "user_id"
    t.text "content"
    t.string "upload_file"
    t.bigint "post_entry_id"
    t.integer "message_type"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["chat_room_id"], name: "index_chat_messages_on_chat_room_id"
    t.index ["created_at"], name: "index_chat_messages_on_created_at"
    t.index ["deleted_at"], name: "index_chat_messages_on_deleted_at"
    t.index ["message_type"], name: "index_chat_messages_on_message_type"
    t.index ["post_entry_id"], name: "index_chat_messages_on_post_entry_id"
    t.index ["updated_at"], name: "index_chat_messages_on_updated_at"
    t.index ["upload_file"], name: "index_chat_messages_on_upload_file"
    t.index ["user_id"], name: "index_chat_messages_on_user_id"
  end

  create_table "chat_rooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "service_identifier"
    t.string "identifier"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_chat_rooms_on_created_at"
    t.index ["deleted_at"], name: "index_chat_rooms_on_deleted_at"
    t.index ["identifier"], name: "index_chat_rooms_on_identifier"
    t.index ["service_identifier"], name: "index_chat_rooms_on_service_identifier"
    t.index ["updated_at"], name: "index_chat_rooms_on_updated_at"
  end

  create_table "contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_contacts_on_created_at"
    t.index ["email"], name: "index_contacts_on_email"
    t.index ["name"], name: "index_contacts_on_name"
    t.index ["updated_at"], name: "index_contacts_on_updated_at"
  end

  create_table "credit_card_archives", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "provider_id"
    t.string "pay_jp_fingerprint"
    t.integer "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_credit_card_archives_on_created_at"
    t.index ["pay_jp_fingerprint"], name: "index_credit_card_archives_on_pay_jp_fingerprint"
    t.index ["provider_id"], name: "index_credit_card_archives_on_provider_id"
    t.index ["reason"], name: "index_credit_card_archives_on_reason"
    t.index ["updated_at"], name: "index_credit_card_archives_on_updated_at"
  end

  create_table "payment_histories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "post_entry_id"
    t.integer "user_id"
    t.string "pay_jp_charge_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_payment_histories_on_created_at"
    t.index ["deleted_at"], name: "index_payment_histories_on_deleted_at"
    t.index ["pay_jp_charge_id"], name: "index_payment_histories_on_pay_jp_charge_id"
    t.index ["post_entry_id"], name: "index_payment_histories_on_post_entry_id"
    t.index ["updated_at"], name: "index_payment_histories_on_updated_at"
    t.index ["user_id"], name: "index_payment_histories_on_user_id"
  end

  create_table "post_entries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "identifier"
    t.integer "post_id"
    t.integer "user_id"
    t.string "service_identifier"
    t.integer "status"
    t.integer "price"
    t.integer "deposited_price"
    t.float "transaction_payment_margin"
    t.datetime "entry_at"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_post_entries_on_created_at"
    t.index ["deleted_at"], name: "index_post_entries_on_deleted_at"
    t.index ["deposited_price"], name: "index_post_entries_on_deposited_price"
    t.index ["entry_at"], name: "index_post_entries_on_entry_at"
    t.index ["identifier"], name: "index_post_entries_on_identifier", unique: true
    t.index ["post_id"], name: "index_post_entries_on_post_id"
    t.index ["price"], name: "index_post_entries_on_price"
    t.index ["service_identifier"], name: "index_post_entries_on_service_identifier"
    t.index ["status"], name: "index_post_entries_on_status"
    t.index ["transaction_payment_margin"], name: "index_post_entries_on_transaction_payment_margin"
    t.index ["updated_at"], name: "index_post_entries_on_updated_at"
    t.index ["user_id"], name: "index_post_entries_on_user_id"
  end

  create_table "post_tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "post_id"
    t.integer "tag_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_post_tags_on_created_at"
    t.index ["deleted_at"], name: "index_post_tags_on_deleted_at"
    t.index ["post_id"], name: "index_post_tags_on_post_id"
    t.index ["tag_id"], name: "index_post_tags_on_tag_id"
    t.index ["updated_at"], name: "index_post_tags_on_updated_at"
  end

  create_table "post_templates", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.text "content", size: :long
    t.string "service_identifier"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_post_templates_on_created_at"
    t.index ["deleted_at"], name: "index_post_templates_on_deleted_at"
    t.index ["service_identifier"], name: "index_post_templates_on_service_identifier"
    t.index ["updated_at"], name: "index_post_templates_on_updated_at"
  end

  create_table "posts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "identifier"
    t.integer "user_id"
    t.string "service_identifier"
    t.string "title"
    t.string "main_image"
    t.integer "category_id"
    t.text "content", size: :long
    t.integer "status", default: 1
    t.integer "price"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_posts_on_category_id"
    t.index ["created_at"], name: "index_posts_on_created_at"
    t.index ["deleted_at"], name: "index_posts_on_deleted_at"
    t.index ["identifier"], name: "index_posts_on_identifier", unique: true
    t.index ["main_image"], name: "index_posts_on_main_image"
    t.index ["price"], name: "index_posts_on_price"
    t.index ["service_identifier"], name: "index_posts_on_service_identifier"
    t.index ["status"], name: "index_posts_on_status"
    t.index ["title"], name: "index_posts_on_title"
    t.index ["updated_at"], name: "index_posts_on_updated_at"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "providers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "pay_jp_customer_id"
    t.string "pay_jp_subscription_id"
    t.string "pay_jp_fingerprint"
    t.string "plan_id"
    t.string "campaign_plan_id"
    t.datetime "trial_expired_at"
    t.text "unsubscribe_reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["campaign_plan_id"], name: "index_providers_on_campaign_plan_id"
    t.index ["confirmation_token"], name: "index_providers_on_confirmation_token", unique: true
    t.index ["email"], name: "index_providers_on_email", unique: true
    t.index ["pay_jp_customer_id"], name: "index_providers_on_pay_jp_customer_id"
    t.index ["pay_jp_fingerprint"], name: "index_providers_on_pay_jp_fingerprint"
    t.index ["pay_jp_subscription_id"], name: "index_providers_on_pay_jp_subscription_id"
    t.index ["plan_id"], name: "index_providers_on_plan_id"
    t.index ["reset_password_token"], name: "index_providers_on_reset_password_token", unique: true
    t.index ["trial_expired_at"], name: "index_providers_on_trial_expired_at"
  end

  create_table "service_contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "service_identifier", null: false
    t.bigint "user_id"
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_service_contacts_on_created_at"
    t.index ["email"], name: "index_service_contacts_on_email"
    t.index ["name"], name: "index_service_contacts_on_name"
    t.index ["service_identifier"], name: "index_service_contacts_on_service_identifier"
    t.index ["updated_at"], name: "index_service_contacts_on_updated_at"
    t.index ["user_id"], name: "index_service_contacts_on_user_id"
  end

  create_table "services", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "identifier"
    t.integer "provider_id"
    t.string "name"
    t.string "post_user"
    t.string "apply_user"
    t.string "post_action"
    t.string "apply_action"
    t.string "post_object"
    t.boolean "transaction_payment"
    t.integer "transaction_payment_type"
    t.boolean "transaction_payment_flat"
    t.integer "transaction_payment_price"
    t.float "transaction_payment_margin"
    t.boolean "subscription_payment"
    t.integer "subscription_payment_limit_type"
    t.string "pay_jp_public_key"
    t.string "pay_jp_private_key"
    t.string "pay_jp_plan_id"
    t.boolean "admin_enable"
    t.integer "admin_type"
    t.text "admin_name"
    t.text "admin_contact"
    t.string "admin_url"
    t.boolean "terms_enable"
    t.text "terms_content"
    t.boolean "privacy_policy_enable"
    t.text "privacy_policy_content"
    t.boolean "commercial_transaction_enable"
    t.text "commercial_transaction_content"
    t.boolean "contact_form_enable"
    t.string "contact_form_mail"
    t.text "mail_signature"
    t.boolean "lp_enable"
    t.text "lp_content"
    t.string "title"
    t.text "description"
    t.string "logo_image"
    t.string "ogp_image"
    t.string "favicon_image"
    t.string "main_color", limit: 20
    t.string "ga_measurement_id", limit: 20
    t.integer "status", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_enable"], name: "index_services_on_admin_enable"
    t.index ["admin_type"], name: "index_services_on_admin_type"
    t.index ["admin_url"], name: "index_services_on_admin_url"
    t.index ["apply_action"], name: "index_services_on_apply_action"
    t.index ["apply_user"], name: "index_services_on_apply_user"
    t.index ["commercial_transaction_enable"], name: "index_services_on_commercial_transaction_enable"
    t.index ["contact_form_enable"], name: "index_services_on_contact_form_enable"
    t.index ["contact_form_mail"], name: "index_services_on_contact_form_mail"
    t.index ["created_at"], name: "index_services_on_created_at"
    t.index ["deleted_at"], name: "index_services_on_deleted_at"
    t.index ["favicon_image"], name: "index_services_on_favicon_image"
    t.index ["ga_measurement_id"], name: "index_services_on_ga_measurement_id"
    t.index ["identifier"], name: "index_services_on_identifier", unique: true
    t.index ["logo_image"], name: "index_services_on_logo_image"
    t.index ["lp_enable"], name: "index_services_on_lp_enable"
    t.index ["main_color"], name: "index_services_on_main_color"
    t.index ["name"], name: "index_services_on_name"
    t.index ["ogp_image"], name: "index_services_on_ogp_image"
    t.index ["pay_jp_plan_id"], name: "index_services_on_pay_jp_plan_id"
    t.index ["pay_jp_private_key"], name: "index_services_on_pay_jp_private_key"
    t.index ["pay_jp_public_key"], name: "index_services_on_pay_jp_public_key"
    t.index ["post_action"], name: "index_services_on_post_action"
    t.index ["post_object"], name: "index_services_on_post_object"
    t.index ["post_user"], name: "index_services_on_post_user"
    t.index ["privacy_policy_enable"], name: "index_services_on_privacy_policy_enable"
    t.index ["provider_id"], name: "index_services_on_provider_id"
    t.index ["status"], name: "index_services_on_status"
    t.index ["subscription_payment"], name: "index_services_on_subscription_payment"
    t.index ["subscription_payment_limit_type"], name: "index_services_on_subscription_payment_limit_type"
    t.index ["terms_enable"], name: "index_services_on_terms_enable"
    t.index ["title"], name: "index_services_on_title"
    t.index ["transaction_payment"], name: "index_services_on_transaction_payment"
    t.index ["transaction_payment_flat"], name: "index_services_on_transaction_payment_flat"
    t.index ["transaction_payment_margin"], name: "index_services_on_transaction_payment_margin"
    t.index ["transaction_payment_price"], name: "index_services_on_transaction_payment_price"
    t.index ["transaction_payment_type"], name: "index_services_on_transaction_payment_type"
    t.index ["updated_at"], name: "index_services_on_updated_at"
  end

  create_table "tags", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.integer "service_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_tags_on_created_at"
    t.index ["deleted_at"], name: "index_tags_on_deleted_at"
    t.index ["name"], name: "index_tags_on_name"
    t.index ["service_id"], name: "index_tags_on_service_id"
    t.index ["updated_at"], name: "index_tags_on_updated_at"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "identifier"
    t.string "service_identifier"
    t.string "email", default: "", null: false
    t.string "name"
    t.string "profile_image"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "pay_jp_customer_id"
    t.string "pay_jp_subscription_id"
    t.string "pay_jp_fingerprint"
    t.string "bank_name"
    t.string "encrypted_bank_name"
    t.string "encrypted_bank_name_iv"
    t.string "bank_code"
    t.string "encrypted_bank_code"
    t.string "encrypted_bank_code_iv"
    t.string "bank_branch_name"
    t.string "encrypted_bank_branch_name"
    t.string "encrypted_bank_branch_name_iv"
    t.string "bank_branch_number"
    t.string "encrypted_bank_branch_number"
    t.string "encrypted_bank_branch_number_iv"
    t.string "bank_account_type"
    t.string "encrypted_bank_account_type"
    t.string "encrypted_bank_account_type_iv"
    t.string "bank_account_number"
    t.string "encrypted_bank_account_number"
    t.string "encrypted_bank_account_number_iv"
    t.string "bank_account_holder"
    t.string "encrypted_bank_account_holder"
    t.string "encrypted_bank_account_holder_iv"
    t.text "unsubscribe_reason"
    t.datetime "unsubscribe_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["bank_account_holder"], name: "index_users_on_bank_account_holder"
    t.index ["bank_account_number"], name: "index_users_on_bank_account_number"
    t.index ["bank_account_type"], name: "index_users_on_bank_account_type"
    t.index ["bank_branch_name"], name: "index_users_on_bank_branch_name"
    t.index ["bank_branch_number"], name: "index_users_on_bank_branch_number"
    t.index ["bank_code"], name: "index_users_on_bank_code"
    t.index ["bank_name"], name: "index_users_on_bank_name"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["encrypted_bank_account_holder"], name: "index_users_on_encrypted_bank_account_holder"
    t.index ["encrypted_bank_account_holder_iv"], name: "index_users_on_encrypted_bank_account_holder_iv"
    t.index ["encrypted_bank_account_number"], name: "index_users_on_encrypted_bank_account_number"
    t.index ["encrypted_bank_account_number_iv"], name: "index_users_on_encrypted_bank_account_number_iv"
    t.index ["encrypted_bank_account_type"], name: "index_users_on_encrypted_bank_account_type"
    t.index ["encrypted_bank_account_type_iv"], name: "index_users_on_encrypted_bank_account_type_iv"
    t.index ["encrypted_bank_branch_name"], name: "index_users_on_encrypted_bank_branch_name"
    t.index ["encrypted_bank_branch_name_iv"], name: "index_users_on_encrypted_bank_branch_name_iv"
    t.index ["encrypted_bank_branch_number"], name: "index_users_on_encrypted_bank_branch_number"
    t.index ["encrypted_bank_branch_number_iv"], name: "index_users_on_encrypted_bank_branch_number_iv"
    t.index ["encrypted_bank_code"], name: "index_users_on_encrypted_bank_code"
    t.index ["encrypted_bank_code_iv"], name: "index_users_on_encrypted_bank_code_iv"
    t.index ["encrypted_bank_name"], name: "index_users_on_encrypted_bank_name"
    t.index ["encrypted_bank_name_iv"], name: "index_users_on_encrypted_bank_name_iv"
    t.index ["identifier"], name: "index_users_on_identifier", unique: true
    t.index ["name"], name: "index_users_on_name"
    t.index ["pay_jp_customer_id"], name: "index_users_on_pay_jp_customer_id"
    t.index ["pay_jp_fingerprint"], name: "index_users_on_pay_jp_fingerprint"
    t.index ["pay_jp_subscription_id"], name: "index_users_on_pay_jp_subscription_id"
    t.index ["profile_image"], name: "index_users_on_profile_image"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["service_identifier", "email"], name: "index_users_on_service_identifier_and_email", unique: true
    t.index ["service_identifier"], name: "index_users_on_service_identifier"
    t.index ["unsubscribe_at"], name: "index_users_on_unsubscribe_at"
  end

end
