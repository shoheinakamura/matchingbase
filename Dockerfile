FROM ruby:2.5.6

RUN apt-get update -qq && \
    apt-get install -y build-essential \
                       libpq-dev \
                       nodejs vim && \
    mkdir /matchingbase

ENV APP_ROOT /matchingbase
WORKDIR $APP_ROOT

ADD ./Gemfile $APP_ROOT/Gemfile
ADD ./Gemfile.lock $APP_ROOT/Gemfile.lock

RUN gem install bundler -v '2.1.3'
RUN bundle install

ADD . $APP_ROOT

CMD ["rails", "server", "-b", "0.0.0.0"]
