module ApplicationHelper
  FLASH_MSG = {
    success: 'alert-success',
    error: 'alert-danger',
    alert: 'alert-danger',
    warning: 'alert-warning',
    notice: 'alert-warning'
  }.freeze

  def flash_class_for(flash_type)
    FLASH_MSG[flash_type.to_sym] || flash_type.to_s
  end

  def active_class(*paths)
    return if paths.blank?
    paths.include?(request.path) ? 'active' : ''
  end

  def convert_url_into_a_tag(text)
    URI.extract(text, ['http', 'https']).uniq.each do |url|
      sub_text = ""
      sub_text << "<a href=" << url << " target=\"_blank\">" << url << "</a>"

      text.gsub!(url, sub_text)
    end
    text
  end

  def create_error_message(error_messages)
    errors = ''
    error_messages.each_with_index do |message, index|
      errors << "#{message[1].present? ? message[1][0] : message}#{'<br>' if index != error_messages.size - 1}"
    end
    errors.html_safe
  end
end
