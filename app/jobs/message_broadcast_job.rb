class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(chat_message)
    chat_room = ChatRoom.find_by(id: chat_message['chat_room_id'])
    user = User.find_by_id(chat_message['user_id'])
    ActionCable.server.broadcast "chat_room_#{chat_room.identifier}", data(chat_message, user)
  end

  private

  def data(chat_message, user)
    {
      own_message_html:
        ApplicationController.renderer.render(
          partial: 'services/chat_room/own_message', locals: {chat_message: chat_message}
        ),
      other_message_html:
        ApplicationController.renderer.render(
          partial: 'services/chat_room/other_message', locals: {chat_message: chat_message}
        ),
      user: user
    }
  end
end
