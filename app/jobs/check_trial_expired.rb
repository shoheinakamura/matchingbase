class CheckTrialExpired
  def self.check
    target_providers = Provider.where(pay_jp_subscription_id: nil).where('trial_expired_at < ?', Time.now)

    target_providers.each do |target_provider|
      if target_provider.pay_jp_customer_id.present? && target_provider.pay_jp_fingerprint.present?
        customer = Payjp::Customer.retrieve(target_provider.pay_jp_customer_id)
        begin
          subscription = Payjp::Subscription.create(customer:customer.id, plan: target_provider.campaign_plan_id.present? ? target_provider.campaign_plan_id : Rails.env.production? ? 'trial_30_cp_1_plan' : 'test_plan_002')
          target_provider.update(pay_jp_subscription_id: subscription.id)
        rescue => e
          target_provider.destroy_all_service
          next
        end
      else
        target_provider.destroy_all_service
      end
    end
  end
end
