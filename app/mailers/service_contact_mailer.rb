class ServiceContactMailer < ApplicationMailer

  def contact_mail(contact, service)
    @contact = contact
    @service = service
    mail to: @service.contact_form_mail, subject: "【#{@service.name}】ユーザーからのお問い合わせ"
  end

  def user_notification_mail(contact, service)
    @contact = contact
    @service = service
    mail to: @contact.email, subject: "【#{@service.name}】お問い合わせを受け付けました"
  end

end
