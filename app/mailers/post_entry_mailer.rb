class PostEntryMailer < ApplicationMailer
  def post_owner_notification_mail(current_user, post, post_entry, chat_room)
    @post = post
    @post_entry = post_entry
    @chat_room = chat_room
    @service = current_user.service
    mail to: @post.user.email, subject: "【マッチングベース】#{@service.apply_action}のお知らせ"
  end
end
