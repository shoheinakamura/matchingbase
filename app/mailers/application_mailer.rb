class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@matchingbase.com'
  layout 'mailer'
end
