class ContactMailer < ApplicationMailer

  def contact_mail(contact)
    @contact = contact
    mail to: 'matchingbase@gmail.com', subject: '【マッチングベース】ユーザーからのお問い合わせ'
  end

  def provider_notification_mail(contact)
    @contact = contact
    mail to: @contact.email, subject: '【マッチングベース】お問い合わせを受け付けました'
  end

end
