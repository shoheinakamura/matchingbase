class ServiceContact < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier', optional: true

  validates :name, presence: true
  validates :email, presence: true, length: {maximum:255},
            format: {with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i}
  validates :message, presence: true
end
