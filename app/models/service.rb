class Service < ApplicationRecord
  acts_as_paranoid

  mount_uploader :logo_image, ServiceLogoImageUploader
  mount_uploader :ogp_image, ServiceOgpImageUploader
  mount_uploader :favicon_image, ServiceFaviconImageUploader

  belongs_to :provider
  has_many :users, foreign_key: 'service_identifier', primary_key: 'identifier' # usersのデータはdestroy時に退会扱いで残す
  has_many :posts, foreign_key: 'service_identifier', primary_key: 'identifier', dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :tags, dependent: :destroy
  has_many :post_templates, foreign_key: 'service_identifier', primary_key: 'identifier', dependent: :destroy
  has_many :chat_rooms, foreign_key: 'service_identifier', primary_key: 'identifier', dependent: :destroy
  has_many :post_entries, foreign_key: 'service_identifier', primary_key: 'identifier', dependent: :destroy

  # 参考：https://gist.github.com/nashirox/38323d5b51063ede1d41
  VALID_PASSWORD_REGEX = /\A[a-z0-9\-\_]+\z/

  validates :name,
            presence: true

  validates :identifier,
            presence: true, uniqueness: true,
            format: { with: VALID_PASSWORD_REGEX },
            length: { maximum: 100 }

  validates :provider_id,
            presence: true

  with_options if: :admin_enable do
    validates :admin_type, presence: true
  end

  with_options if: :contact_form_enable do
    validates :contact_form_mail, presence: true
  end

  before_save :update_identifier_relation

  # 0:投稿者が課金, 1:応募者が課金 ※0の場合は応募者が、1の場合は投稿者が金額を設定する
  enum transaction_payment_type: { paid_by_post_user: 0, paid_by_apply_user: 1 }

  # 0:投稿のみ, 1:応募のみ, 2:投稿と応募
  # TODO チャット機能にも制限をかけるべきか検討
  enum subscription_payment_limit_type: { limit_post: 0, limit_apply: 1, limit_post_and_apply: 2 }

  # 0:内部リンク, 1:外部リンク
  enum admin_type: { internal_link: 0, external_link: 1 }

  # 0:非公開, 1:公開, 2:停止
  enum status: { pending: 0, released: 1, stoped: 2 }

  def reset_users_payment_info
    self.users.each do |user|
      user.reset_payment_info
    end
  end

  def delete_users_subscription
    self.users.each do |user|
      user.delete_subscription
    end
  end

  private

  def update_identifier_relation
    return unless self.identifier_changed?
    PostTemplate.where(service_identifier: self.identifier_was).update_all(service_identifier: self.identifier)
  end
end
