class Category < ApplicationRecord
  acts_as_paranoid

  has_many   :posts, dependent: :nullify
  belongs_to :service
end
