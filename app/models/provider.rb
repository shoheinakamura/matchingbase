class Provider < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :trackable

  has_many :services
  has_many :credit_card_archives

  def destroy_all_service
    self.services.each do |service|
      # ユーザー退会処理
      service.users.each do |user|
        user.unsubscribe
      end
      service.destroy
    end
  end
end
