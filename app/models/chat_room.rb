class ChatRoom < ApplicationRecord
  acts_as_paranoid

  has_many   :chat_messages, dependent: :destroy
  has_many   :chat_members, dependent: :destroy
  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier'

  def self.create_chat_room(post, current_user)
    post_user_chat_room_ids = ChatMember.where(user_id: post.user.id).pluck(:chat_room_id)
    chat_member = ChatMember.where(user_id: current_user.id).where(chat_room_id: post_user_chat_room_ids).first
    if chat_member.present?
      chat_room = ChatRoom.find_by(id: chat_member.chat_room_id)
    else
      chat_room = ChatRoom.create(
        service_identifier: current_user.service_identifier,
        identifier: Time.now.strftime('%Y%m%d%H%M%S') + SecureRandom.random_number(10**4).to_s
      )
      ChatMember.create(
        chat_room_id: chat_room.id,
        user_id: post.user.id
      )
      ChatMember.create(
        chat_room_id: chat_room.id,
        user_id: current_user.id
      )
    end
    chat_room
  end
end
