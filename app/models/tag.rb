class Tag < ApplicationRecord
  acts_as_paranoid

  has_many   :post_tags, dependent: :destroy
  belongs_to :service
end
