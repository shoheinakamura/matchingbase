class CreditCardArchive < ApplicationRecord
  belongs_to :provider

  # 0:退会,1:クレジットカード変更
  enum reason: { unsubscribe: 0, change_credit_card: 1 }
end
