class PostEntry < ApplicationRecord
  acts_as_paranoid

  belongs_to :post
  belongs_to :user
  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier'
  has_many :payment_histories
  has_many :chat_messages

  # 0:停止, 1:進行中, 2:支払待ち, 3:支払済, 4:入金済, 5:完了
  enum status: { stop: 0, in_progress: 1, unpaid: 2, paid: 3, deposited: 4, completed: 5 }

  validates :identifier,
            presence: true, uniqueness: true

  with_options if: :is_unpaid? do
    validates :price, presence: true, numericality: { only_integer: true }
    validate :price_over_50
  end

  def price_over_50
    if self.price.blank? || self.price < 50
      errors.add(:price, '価格は50円以上に設定してください。')
    end
  end

  def is_unpaid?
    self.unpaid?
  end
end
