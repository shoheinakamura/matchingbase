class ChatMember < ApplicationRecord
  acts_as_paranoid

  belongs_to :chat_room
  belongs_to :user
end
