class PostTemplate < ApplicationRecord
  acts_as_paranoid

  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier'
end
