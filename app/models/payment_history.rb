class PaymentHistory < ApplicationRecord
  acts_as_paranoid

  belongs_to :post_entry
  belongs_to :user
end
