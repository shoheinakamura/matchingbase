class User < ApplicationRecord
  mount_uploader :profile_image, UserProfileImageUploader

  has_many :posts
  has_many :chat_messages
  has_many :chat_members
  has_many :post_entries
  has_many :payment_histories
  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier', optional: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :trackable,
         authentication_keys: [:service_identifier, :email],
         confirmation_keys: [:service_identifier, :email],
         reset_password_keys: [:service_identifier, :email]

  attr_encrypted :bank_name, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_code, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_branch_name, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_branch_number, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_account_type, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_account_number, key: Rails.application.credentials.dig(:attr_encrypted, :key)
  attr_encrypted :bank_account_holder, key: Rails.application.credentials.dig(:attr_encrypted, :key)

  # acts_as_paranoid
  #
  # validates :email,
  #           uniqueness_without_deleted: true,
  #           email_format: true

  validates :service_identifier, presence: true
  validates :name, presence: true
  # validates :name, presence: true, if: :is_update?
  # validates :bank_code, format: {with: /\A[0-9]{7}\z/}
  # validates :bank_branch_number, format: {with: /\A[0-9]{4}\z/}

  validates_uniqueness_of :email, scope: [:service_identifier]

  # def is_update?
  #   self.id.present?
  # end

  # 登録時にemailを不要とする
  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  def blank_bank_info?
    self.bank_name.blank? ||
      self.bank_code.blank? ||
      self.bank_branch_name.blank? ||
      self.bank_branch_number.blank? ||
      self.bank_account_type.blank? ||
      self.bank_account_number.blank? ||
      self.bank_account_holder.blank?
  end

  def exist_unread_chat_message?
    self.chat_members.each do |chat_member|
      chat_member.chat_room.chat_messages.where.not(user_id: self.id).each do |chat_message|
        if chat_member.last_read_at.present? && chat_member.last_read_at < chat_message.created_at
          return true
        end
      end
    end
    false
  end

  #
  # validates :password,
  #           length: { minimum: 8 },
  #           confirmation: true,
  #           if: :new_record_or_changes_password
  #
  # validates :password_confirmation,
  #           presence: true,
  #           if: :new_record_or_changes_password
  #
  # private
  #
  # def new_record_or_changes_password
  #   new_record? || changes[:crypted_password]
  # end

  def unsubscribe(unsubscribe_reason: nil)
    # クレジットカード情報の削除
    reset_payment_info

    # 募集アクションの停止
    self.posts.update_all(status: Post.statuses[:stop])

    unsubscribe_email = self.email << '.unsubscribe' << Time.now.strftime('%Y%m%d%H%M%S')
    if User.find_by(email: unsubscribe_email, service_identifier: self.service.identifier).blank?
      if self.update(
        email: unsubscribe_email,
        name: '退会ユーザー',
        unsubscribe_reason: unsubscribe_reason,
        unsubscribe_at: Time.now
      )
        {result: true, message: '退会しました。ご利用いただきましてありがとうございました。（登録情報を削除しました。）'}
      else
        {result: false, message: '退会処理でエラーが発生しました。お問い合わせフォームよりお問い合わせください。エラーコード03'}
      end
    else
      {result: false, message: '退会処理でエラーが発生しました。お問い合わせフォームよりお問い合わせください。エラーコード02'}
    end
  end

  def reset_payment_info
    if self.pay_jp_customer_id.present?
      # クレジットカード削除
      Payjp.api_key = self.service.pay_jp_private_key
      customer = Payjp::Customer.retrieve(self.pay_jp_customer_id)
      card = customer.cards.retrieve(customer.cards.all().data.first.id)
      card.delete
      customer.delete

      # user情報リセット
      self.update(
        pay_jp_customer_id: nil,
        pay_jp_subscription_id: nil,
        pay_jp_fingerprint: nil
      )
    end
  end

  def delete_subscription
    if self.pay_jp_subscription_id.present?
      Payjp.api_key = self.service.pay_jp_private_key
      pay_jp_subscription = Payjp::Subscription.retrieve(self.pay_jp_subscription_id)
      if pay_jp_subscription.present?
        if pay_jp_subscription.delete
          self.update(pay_jp_subscription_id: nil)
          true
        else
          false
        end
      else
        false
      end
    end
  end

  def unsubscribed?
    self.unsubscribe_at.present?
  end
end
