class Post < ApplicationRecord
  acts_as_paranoid

  mount_uploader :main_image, PostImageUploader

  belongs_to :user
  belongs_to :service, foreign_key: 'service_identifier', primary_key: 'identifier', optional: true
  belongs_to :category, optional: true
  has_many :post_tags, dependent: :destroy
  has_many :post_entries, dependent: :destroy

  attr_reader :tag_ids

  # 0:停止, 1:進行中
  enum status: { stop: 0, in_progress: 1 }

  validates :identifier,
            presence: true, uniqueness: true

  validates :service_identifier,
            presence: true

  validates :title,
            presence: true

  validates :content,
            presence: true,
            length: { maximum: 4294967295 }
end
