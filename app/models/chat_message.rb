class ChatMessage < ApplicationRecord
  acts_as_paranoid

  belongs_to :chat_room
  belongs_to :user, optional: true
  belongs_to :post_entry, optional: true

  after_create :update_chat_room_updated_at

  mount_uploader :upload_file, ChatMessageFileUploader

  # 0:テキスト, 1:ファイル, 2:通知
  enum message_type: { text_message: 0, file_message: 1, information_message: 2 }

  validates :content, presence: true, if: :text_message?

  #参考 https://railsguides.jp/active_record_callbacks.html
  after_commit { MessageBroadcastJob.perform_later self }

  private

  def update_chat_room_updated_at
    self.chat_room.update(updated_at: Time.now)
  end
end
