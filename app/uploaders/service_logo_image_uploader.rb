class ServiceLogoImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog
  cache_storage :fog

  def extension_white_list
    %w[png jpeg jpg svg]
  end

  def store_dir
    Rails.env.production? ? "service_logo_image/#{model.identifier}" : "develop_service_logo_image/#{model.identifier}"
  end

  def cache_dir
    Rails.env.production? ? "service_logo_image/cache" : "develop_service_logo_image/cache"
  end

  def filename
    original_filename if original_filename
  end

  def auto
    manipulate! do|image|
      image.auto_orient
    end
  end

  process resize_to_limit: [300, 500], if: :is_not_svg?
  process :auto, if: :is_not_svg?

  def is_not_svg?(picture)
    picture.content_type != 'image/svg+xml'
  end
end
