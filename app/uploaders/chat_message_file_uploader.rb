class ChatMessageFileUploader < CarrierWave::Uploader::Base
  storage :fog
  cache_storage :fog

  def store_dir
    Rails.env.production? ? "chat_message_file/#{model.id}" : "develop_chat_message_file/#{model.id}"
  end

  def cache_dir
    Rails.env.production? ? "chat_message_file/cache" : "develop_chat_message_file/cache"
  end

  def filename
    original_filename if original_filename
  end

  def size_range
    1..10.megabytes
  end
end
