class UserProfileImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog
  cache_storage :fog

  def extension_white_list
    %w[png jpeg jpg]
  end

  def store_dir
    Rails.env.production? ? "user_profile_image/#{model.identifier}" : "develop_user_profile_image/#{model.identifier}"
  end

  def cache_dir
    Rails.env.production? ? "user_profile_image/cache" : "develop_user_profile_image/cache"
  end

  def filename
    original_filename if original_filename
  end

  process resize_to_limit: [1000, 1000]

  def auto
    manipulate! do|image|
      image.auto_orient
    end
  end

  process :auto
end
