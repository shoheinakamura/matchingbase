class ServiceFaviconImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog
  cache_storage :fog

  def extension_white_list
    %w[png jpeg jpg]
  end

  def store_dir
    Rails.env.production? ? "service_favicon_image/#{model.identifier}" : "develop_service_favicon_image/#{model.identifier}"
  end

  def cache_dir
    Rails.env.production? ? "service_favicon_image/cache" : "develop_service_favicon_image/cache"
  end

  def filename
    "#{File.basename("#{original_filename}", ".*")}.ico" if original_filename
  end

  process resize_to_fill: [48, 48, 'Center']
  process convert: 'ico'

  def auto
    manipulate! do|image|
      image.auto_orient
    end
  end

  process :auto
end
