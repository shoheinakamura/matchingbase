class PostContentImageUploader
  def initialize(upload_image)
    img = MiniMagick::Image.read(MiniMagick::Image.open(upload_image.tempfile)).resize '1000x1000>'
    @file = img.tempfile
    folder_name = Rails.env.production? ? 'post_content_image' : 'develop_post_content_image'
    @key_name = "#{folder_name}/#{SecureRandom.hex(4)}#{Time.now.strftime("%Y%m%d%H%M%S")}"
    @s3 = Aws::S3::Resource.new(
      region: 'ap-northeast-1',  # リージョン東京
      credentials: Aws::Credentials.new(
        Rails.application.credentials.dig(:aws_s3, :access_key_id),  # S3用アクセスキー
        Rails.application.credentials.dig(:aws_s3, :secret_access_key)  # S3用シークレットアクセスキー
      )
    )
  end

  def upload_image
    begin
      @s3.bucket(get_bucket_name).object(get_key_name).put(body: @file)
      return true
    rescue StandardError => e
      puts "アップロードに失敗しました：#{e}"
      return false
    end
  end

  def get_bucket
    @s3.bucket(get_bucket_name)
  end

  def get_key_name
    @key_name
  end

  private

  def get_bucket_name
    'matchingbase'
  end
end
