class PostImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog
  cache_storage :fog

  def extension_white_list
    %w[png jpeg jpg pdf]
  end

  def store_dir
    Rails.env.production? ? "post_main_image/#{model.id}" : "develop_post_main_image/#{model.id}"
  end

  def cache_dir
    Rails.env.production? ? "post_main_image/cache" : "develop_post_main_image/cache"
  end

  def filename
    original_filename if original_filename
  end

  process resize_to_limit: [1000, 1000]

  def auto
    manipulate! do|image|
      image.auto_orient
    end
  end

  process :auto
end
