class ServiceOgpImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog
  cache_storage :fog

  def extension_white_list
    %w[png jpeg jpg]
  end

  def store_dir
    Rails.env.production? ? "service_ogp_image/#{model.identifier}" : "develop_service_ogp_image/#{model.identifier}"
  end

  def cache_dir
    Rails.env.production? ? "service_ogp_image/cache" : "develop_service_ogp_image/cache"
  end

  def filename
    original_filename if original_filename
  end

  process resize_to_limit: [300, 500]

  def auto
    manipulate! do|image|
      image.auto_orient
    end
  end

  process :auto
end
