class ServiceTermsController < ApplicationController
  layout 'service_lps/layouts/application'

  def show
    @noindex = true
  end
end
