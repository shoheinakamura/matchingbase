class ManagePaymentsController < ProvidersController
  def index
    if params[:service_identifier].present?
      @service = current_provider.services.find_by(identifier: params[:service_identifier])
    else
      @service = current_provider.services.first
    end

    if @service.present?
      @post_entries = @service.post_entries.where(status: [:paid, :deposited]).order(updated_at: :desc)

      if params[:from_date].present?
        @post_entries = @post_entries.where('entry_at >= ?', params[:from_date])
      end
      if params[:to_date].present?
        @post_entries = @post_entries.where('entry_at <= ?', params[:to_date])
      end
      if params[:status].present?
        @post_entries = @post_entries.where('status = ?', params[:status])
      end
    end
  end

  def update
    post_entry = PostEntry.find_by(identifier: params[:identifier])
    if post_entry.present? && params[:status].present?
      post_entry.update(
        status: params[:status].to_i,
        deposited_price: (post_entry.price * (1 - post_entry.transaction_payment_margin*0.01)).to_i
      )
      redirect_to manage_payments_path(service_identifier: post_entry.service.identifier), notice: '更新しました。'
    else
      flash[:alert] = '更新に失敗しました'
      render :index
    end
  end
end
