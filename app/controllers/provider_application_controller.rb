class ProviderApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :require_login
  before_action :set_pay_jp

  def after_sign_in_path_for(resource)
    root_path
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def require_login
    redirect_to new_provider_session_path unless provider_signed_in?
  end

  def set_pay_jp
    Payjp.api_key = if Rails.env.production?
                      Rails.application.credentials.payjp[:private_key]
                    else
                      Rails.application.credentials.payjp[:test_private_key]
                    end
  end

  def create_error_message(error_messages)
    errors = ''
    error_messages.each_with_index do |message, index|
      errors << "#{message[1].present? ? message[1][0] : message}#{'<br>' if index != error_messages.size - 1}"
    end
    errors.html_safe
  end

end
