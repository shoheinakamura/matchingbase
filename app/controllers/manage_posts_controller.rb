class ManagePostsController < ProvidersController

  def index
    if params[:service_identifier].present?
      @service = current_provider.services.find_by(identifier: params[:service_identifier])
    else
      @service = current_provider.services.first
    end
    if @service.present?
      @posts = @service.posts.order(updated_at: :desc)

      if params[:from_date].present?
        @posts = @posts.where('updated_at >= ?', params[:from_date])
      end
      if params[:to_date].present?
        @posts = @posts.where('updated_at <= ?', params[:to_date])
      end
      if params[:title].present?
        @posts = @posts.where('title LIKE ?',"%#{params[:title]}%")
      end
    end
  end

  def edit
    @post = Post.find_by(identifier: params[:identifier])
  end

  def update
    @post = Post.find_by(identifier: params[:identifier])

    if @post.update(post_params)
      redirect_to edit_manage_post_path(identifier: @post.identifier), flash: {success: '更新しました。'}
    else
      flash[:error] = '更新に失敗しました'
      render :edit
    end
  end

  def destroy
    @post = Post.find_by(identifier: params[:identifier])

    if @post.post_entries.present?
      redirect_to manage_posts_path, flash: {error: "#{@post.service.apply_action}が存在するため削除できません。"} and return
    end

    if @post.destroy
      redirect_to manage_posts_path, flash: {success: '削除しました。'}
    else
      redirect_to manage_posts_path, flash: {error: '削除できませんでした。'}
    end
  end

  private

  def post_params
    params.require(:post).permit(
      :title,
      :main_image,
      :content
    )
  end

end
