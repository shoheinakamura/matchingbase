class ManageServicesController < ProvidersController

  def index
  end

  def show
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name}
    )
  end

  def new
    @service = Service.new
  end

  def create
    @service = Service.new(new_service_params)

    # 重複チェック
    if Service.with_deleted.find_by(identifier: new_service_params[:identifier]).present?
      flash.now[:error] = '指定のサービスIDはすでに利用されているため設定できません。'
      render :new and return
    end

    @service.assign_attributes(
      provider_id: current_provider.id
    )
    if @service.save
      redirect_to edit_manage_service_matching_path(identifier: @service.identifier, from_new: true)
    else
      flash.now[:error] = create_error_message(@service.errors.messages)
      render :new
    end
  end

  def edit
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service

    if @service.update(update_service_params)
      flash[:success] = '更新しました'
      redirect_to edit_manage_service_path(identifier: @service.identifier)
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  def destroy
    @service = Service.find_by(identifier: params[:identifier])

    # ユーザー退会処理
    @service.users.each do |user|
      user.unsubscribe
    end

    if @service.destroy
      flash[:success] = '削除しました'
      render :index
    else
      flash[:alert] = '削除できませんでした'
      render :index
    end
  end

  private

  def new_service_params
    params.require(:service).permit(
      :name,
      :identifier
    )
  end

  def update_service_params
    params.require(:service).permit(
      :name
    )
  end

end
