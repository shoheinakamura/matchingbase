class ManageServiceUisController < ProvidersController

  def edit
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service_ui

    if @service.update(service_params)
      flash[:success] = '更新しました'
      if ActiveRecord::Type::Boolean.new.cast(params[:from_new])
        redirect_to edit_manage_service_release_path(identifier: @service.identifier, from_new: params[:from_new])
      else
        redirect_to edit_manage_service_ui_path(identifier: @service.identifier)
      end
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  private

  def service_params
    params.require(:service).permit(
      :title,
      :description,
      :logo_image,
      :logo_image_cache,
      :ogp_image,
      :ogp_image_cache,
      :favicon_image,
      :favicon_image_cache,
      :main_color,
      :ga_measurement_id
    )
  end

end
