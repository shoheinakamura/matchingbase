class Services::ContactsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) and return unless @service.contact_form_enable
    @service_contact = ServiceContact.new
  end

  def create
    return unless @service.contact_form_enable

    @service_contact = ServiceContact.new(service_contact_params)
    @service_contact.assign_attributes(
      service_identifier: @service.identifier,
      user_id: current_user.present? ? current_user.id : nil
    )
    if @service_contact.save
      ServiceContactMailer.contact_mail(@service_contact, @service).deliver
      ServiceContactMailer.user_notification_mail(@service_contact, @service).deliver
      flash[:success] = 'お問い合わせを受け付けました。'
      redirect_to service_contact_path(@service.identifier)
    else
      render :show
    end
  end

  private

  def service_contact_params
    params.require(:service_contact).permit(:name, :email, :message)
  end
end
