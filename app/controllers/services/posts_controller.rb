class Services::PostsController < ApplicationController
  before_action :require_login, except: %i[index show search]

  def index
  end

  def show
    @post = Post.find_by(identifier: params[:identifier])
  end

  def new
    # サブスクリプション制限判定
    if @service.subscription_payment?
      if @service.limit_post? || @service.limit_post_and_apply?
        if current_user.pay_jp_customer_id.blank? || current_user.pay_jp_fingerprint.blank? || current_user.pay_jp_subscription_id.blank?
          redirect_to service_users_plan_path, alert: "#{@service.post_action}するにはプランに登録してください。" and return
        else
          pay_jp_subscription = Payjp::Subscription.retrieve(current_user.pay_jp_subscription_id)
          if pay_jp_subscription.status == 'canceled'
            redirect_to service_users_plan_path, alert: "#{@service.post_action}するにはプランに登録してください。" and return
          end
        end
      end
    end

    # トランザクション制限判定
    if @service.transaction_payment?
      if @service.paid_by_post_user?
        # クレジットカード登録チェック
        if current_user.pay_jp_customer_id.blank? || current_user.pay_jp_fingerprint.blank?
          redirect_to service_users_payment_path, alert: "#{@service.post_action}するにはクレジットカードを登録してください。" and return
        end
      elsif @service.paid_by_apply_user?
        if current_user.blank_bank_info?
          redirect_to service_users_bank_path, alert: "#{@service.post_action}するには料金を受け取る口座情報を登録してください。" and return
        end
      end
    end
    @post = Post.new
  end

  def create
    @post = Post.create(post_params)
    @post.assign_attributes(
      identifier: Time.now.strftime('%Y%m%d%H%M%S') + SecureRandom.random_number(10**4).to_s,
      user_id: current_user.id,
      service_identifier: current_user.service_identifier
    )
    @post_tag_ids = post_tags_params[:tag_ids]&.reject(&:empty?)
    if @post.save
      # update post_tags
      @post_tag_ids.each do |tag_id|
        PostTag.create(post_id: @post.id, tag_id: tag_id)
      end if @post_tag_ids.present?
      redirect_to service_post_path(service_identifier: @service.identifier, identifier: @post.identifier)
    else
      render :new
    end
  end

  def edit
    @post = Post.find_by(identifier: params[:identifier])
    @post_tag_ids = @post.post_tags.pluck(:tag_id)
    render :show unless @post.user == current_user
  end

  def update
    @post = Post.find_by(identifier: params[:identifier])
    @post_tag_ids = post_tags_params[:tag_ids]&.reject(&:empty?)

    @post.assign_attributes(post_params)

    if @post.save
      # update post_tags
      PostTag.where(post_id: @post.id).delete_all
      @post_tag_ids.each do |tag_id|
        PostTag.create(post_id: @post.id, tag_id: tag_id)
      end if @post_tag_ids.present?
      redirect_to service_post_path(service_identifier: @service.identifier, identifier: @post.identifier), flash: {success: '更新しました。'}
    else
      render :new
    end
  end

  def destroy
    @post = Post.find_by(identifier: params[:identifier])
    if @post.post_entries.present?
      redirect_to service_post_path(service_identifier: @service.identifier, identifier: @post.identifier), alert: "#{@service.apply_action}が存在するため削除できません。" and return
    end
    if @post.destroy
      redirect_to service_path(identifier: @service.identifier), notice: '削除しました。'
    else
      render :edit
    end
  end

  def change_status
    @post = Post.find_by(identifier: params[:identifier])

    @post.assign_attributes(post_params)

    # 停止の場合は日付を更新しない
    @post.record_timestamps = false if @post.stop?

    if @post.save
      message = ''
      if @post.stop?
        message = '停止しました。'
      elsif @post.in_progress?
        message = '再開しました。'
      end
      redirect_to service_post_path(service_identifier: @service.identifier, identifier: @post.identifier), notice: message
    else
      render :new
    end
  end

  def upload_image
    image_uploader = PostContentImageUploader.new(params[:upload_image])
    if image_uploader.upload_image
      object = image_uploader.get_bucket.object(image_uploader.get_key_name)
      render( json: { url: URI.parse(object.public_url) } )
    else
      head 400
    end
  end

  def insert_post_template
    post_template = @service.post_templates&.first
    render( json: { content: post_template.content } )
  end

  def search
    #NOTE https://qiita.com/nysalor/items/9a95d91f2b97a08b96b0
    search_words = params[:search_word].split
    # ransack_results = Post.includes(:categories, :tags)
    #            .search(title_or_content_or_category_name_cont_all: search_words)
    #            .result.page(params[:page]).per(10)
    ransack_results = Post.includes(:category, :post_tags).where(service_identifier: @service.identifier)
                        .search(title_or_content_or_category_name_cont_all: search_words).result
    where_string = ''
    search_words.each do |search_word|
      if where_string.blank?
        where_string << "CONCAT(title, content) LIKE '%#{search_word}%' "
      else
        where_string << "AND CONCAT(title, content) LIKE '%#{search_word}%' "
      end
    end
    if where_string.present?
      where_string << "OR id IN (?)"
    else
      where_string << "id IN (?)"
    end
    @posts = Post.where(service_identifier: @service.identifier)
               .where(where_string, ransack_results.pluck(:id))
    # search category
    if params[:category_ids].present?
      @posts = @posts.where(category_id: params[:category_ids])
    end
    # search tag
    if params[:tag_ids].present?
      post_tags = PostTag.where(tag_id: params[:tag_ids])
      @posts = @posts.where(id: post_tags.pluck(:post_id))
    end
    # status
    if params[:post_status].present?
      @posts = @posts.where(status: params[:post_status])
    end

    @posts = @posts.order(updated_at: :DESC).page(params[:page]).per(10)
    @params = params
  end

  private

  def post_params
    params.require(:post).permit(:title, :main_image, :main_image_cache, :content, :status, :price, :category_id)
  end

  def post_tags_params
    params.require(:post).permit(tag_ids: [])
  end

  # def require_login
  #   redirect_to root_path unless user_signed_in?
  # end
end
