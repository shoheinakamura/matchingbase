class Services::ChatRoomController < ApplicationController
  before_action :require_login

  def index
    redirect_to service_path(current_user.service_identifier) and return if current_user.chat_members.blank?

    # 一覧ではなく最新の詳細を表示
    chat_room_ids = current_user.chat_members.pluck(:chat_room_id).uniq
    @chat_rooms = ChatRoom.where(id: chat_room_ids).order(updated_at: :desc)
    @chat_room = @chat_rooms.first
    @chat_messages = @chat_room.chat_messages

    chat_member = @chat_room.chat_members.find_by(user_id: current_user.id)
    # 新着メッセージタグ付与用
    @unread_message_ids = if chat_member.last_read_at.present?
                            @chat_messages.where('CAST(created_at AS DATETIME) > ?', chat_member.last_read_at).pluck(:id)
                          else
                            @chat_messages.pluck(:id)
                          end
    if chat_member.present?
      chat_member.update(last_read_at: Time.now)
    end

    render :show
  end

  def show
    redirect_to service_path(current_user.service_identifier) and return if current_user.chat_members.blank?

    chat_room_ids = current_user.chat_members.pluck(:chat_room_id).uniq
    @chat_rooms = ChatRoom.where(id: chat_room_ids).order(updated_at: :desc)
    @chat_room = ChatRoom.find_by(identifier: params[:identifier])
    @chat_messages = @chat_room.chat_messages

    chat_member = @chat_room.chat_members.find_by(user_id: current_user.id)
    # 新着メッセージタグ付与用
    @unread_message_ids = if chat_member.last_read_at.present?
                            @chat_messages.where('CAST(created_at AS DATETIME) > ?', chat_member.last_read_at).pluck(:id)
                          else
                            @chat_messages.pluck(:id)
                          end
    if chat_member.present?
      chat_member.update(last_read_at: Time.now)
    end
  end

  def create
    post = Post.find_by(identifier: params[:post_identifier])
    chat_room = ChatRoom.create_chat_room(post, current_user)
    redirect_to service_chat_room_path(service_identifier: current_user.service_identifier, identifier: chat_room.identifier)
  end

  def upload_file
    chat_room = ChatRoom.find_by(identifier: params[:chat_room_identifier], service_identifier: params[:service_identifier])
    if chat_room.present?
      ChatMessage.create(
        content: '',
        upload_file: params[:upload_file],
        chat_room_id: chat_room.id,
        message_type: ChatMessage.message_types[:file_message],
        user_id: current_user.id
      )
      render( json: { res: true } )
    else
      render( json: { res: false } )
    end
  end

  def download_file
    chat_message = ChatMessage.find_by_id(params[:chat_message_id])
    upload_file = chat_message.upload_file
    send_data(upload_file.read, disposition: 'attachment', filename: upload_file.file.filename, type: upload_file.content_type)
  end

  def update_last_read_at
    chat_room = ChatRoom.find_by(identifier: params[:chat_room_identifier])
    if chat_room.present?
      chat_member = chat_room.chat_members.find_by(user_id: params[:user_id])
      chat_member.update(last_read_at: Time.now) if chat_member.present?
    end
  end

end
