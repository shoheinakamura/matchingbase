class Services::PostEntriesController < ApplicationController
  before_action :require_login

  def index
    @post_entries = current_user.post_entries.order(updated_at: :desc)
  end

  def show
    @post_entry = PostEntry.find_by(identifier: params[:identifier])
  end

  def create
    # サブスクリプション制限判定
    if @service.subscription_payment?
      if @service.limit_apply? || @service.limit_post_and_apply?
        if current_user.pay_jp_customer_id.blank? || current_user.pay_jp_fingerprint.blank?
          redirect_to service_users_payment_path, alert: "#{@service.apply_action}するにはクレジットカードを登録してください。" and return
        end
        if current_user.pay_jp_subscription_id.blank?
          redirect_to service_users_plan_path, alert: "#{@service.apply_action}するにはプランに登録してください。" and return
        else
          pay_jp_subscription = Payjp::Subscription.retrieve(current_user.pay_jp_subscription_id)
          if pay_jp_subscription.status == 'canceled'
            redirect_to service_users_plan_path, alert: "#{@service.apply_action}するにはプランに登録してください。" and return
          end
        end
      end
    end

    # トランザクション制限判定
    if @service.transaction_payment?
      if @service.paid_by_apply_user?
        # クレジットカード登録チェック
        if current_user.pay_jp_customer_id.blank? || current_user.pay_jp_fingerprint.blank?
          redirect_to service_users_payment_path, alert: "#{@service.apply_action}するにはクレジットカードを登録してください。" and return
        end

        # TODO 以下の与信確認をすべきかは検討したい
        # begin
        #   customer = Payjp::Customer.retrieve(current_user.pay_jp_customer_id)
        #   card = customer.cards.retrieve(customer.cards.all().data.first.id)
        #   charge = Payjp::Charge.create(
        #     amount: 100, #与信のため100円で実行する
        #     customer: customer.id,
        #     card: card.id,
        #     capture: false,
        #     currency: 'jpy',
        #     description: '与信チェック'
        #   )
        # rescue
        #   redirect_to service_users_mypage_path, alert: '有効なクレジットカードが登録されていないためカード情報を更新してください。' and return
        # end
        #
        # # 与信のためキャンセルする
        # charge.refund
      elsif @service.paid_by_post_user?
        if current_user.blank_bank_info?
          redirect_to service_users_bank_path, alert: "#{@service.apply_action}するには料金を受け取る口座情報を登録してください。" and return
        end
      end
    end

    post = Post.find_by(identifier: params[:post_identifier])

    # 投稿停止中の場合はreturn
    redirect_to service_post_path(service_identifier: @service.identifier, identifier: post.identifier), alert: "停止中のため#{@service.apply_action}できません。" and return if post.stop?

    exist_post_entry = PostEntry.where.not(status: :completed).where(
      user_id: current_user.id,
      post_id: post.id
    )
    if exist_post_entry.blank?
      post_entry = PostEntry.new(
        user_id: current_user.id,
        identifier: Time.now.strftime('%Y%m%d%H%M%S') + SecureRandom.random_number(10**4).to_s,
        post_id: post.id,
        service_identifier: @service.identifier,
        status: PostEntry.statuses[:in_progress],
        transaction_payment_margin: @service.transaction_payment_margin,
        entry_at: Time.now
      )
      if post_entry.save
        chat_room = ChatRoom.create_chat_room(post, current_user)
        ChatMessage.create(
          chat_room_id: chat_room.id,
          user_id: current_user.id,
          message_type: ChatMessage.message_types[:information_message],
          post_entry_id: post_entry.id
        )
        PostEntryMailer.post_owner_notification_mail(current_user, post, post_entry, chat_room).deliver
        redirect_to service_chat_room_path(service_identifier: @service.identifier, identifier: chat_room.identifier), flash: {warning: "#{@service.apply_action}しました。"}
      end
    else
      redirect_to service_post_path(service_identifier: @service.identifier, identifier: post.identifier), flash: {warning: "#{@service.apply_action}済みです。"}
    end
  end

  def update
    @post_entry = PostEntry.find_by(identifier: params[:identifier])
    # TODO 設定値ごとの制御を書く
    # transaction_paymentの場合：statusは「stop・in_progress・completed」なので「unpaid・paid・deposited」がリクエストされた場合はエラーを返すなど
    if @post_entry.update(post_entry_params)
      flash[:success] = '更新しました'
      redirect_to service_post_entry_path(service_identifier: current_user.service_identifier, identifier: @post_entry.identifier)
    else
      @post_entry.reload
      flash.now[:alert] = create_error_message(@post_entry.errors.messages)
      render :show
    end
  end

  private

  def post_entry_params
    params.require(:post_entry).permit(:status, :price)
  end

end
