class Services::OwnPostsController < ApplicationController
  before_action :require_login

  def show
    @own_posts = current_user.posts.includes(:post_entries).order(updated_at: :desc)
  end

end
