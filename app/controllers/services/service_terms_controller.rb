class Services::ServiceTermsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.terms_enable
  end
end
