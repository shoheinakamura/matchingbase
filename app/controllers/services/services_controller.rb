class Services::ServicesController < ApplicationController
  def show
    @posts = Post.where(service_identifier: @service.identifier).all.order(updated_at: :DESC).page(params[:page]).per(10)
  end
end
