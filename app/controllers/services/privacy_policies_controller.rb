class Services::PrivacyPoliciesController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.privacy_policy_enable
  end
end
