class Services::CommercialTransactionsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.commercial_transaction_enable
  end
end
