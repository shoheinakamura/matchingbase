class Services::ServiceAdminsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.admin_enable
  end
end
