class Services::ServiceLpsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.lp_enable
  end
end
