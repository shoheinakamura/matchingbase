class Services::Users::BanksController < ApplicationController
  def show
    # 未ログインまたはトランザクション課金設定がない場合リダイレクト
    if current_user.blank? || !@service.transaction_payment?
      redirect_to service_path(identifier: @service.identifier) and return
    end

    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(user_params)
      redirect_to service_users_bank_path, notice: 'ご登録情報を更新しました。'
    else
      flash.now[:error] = '入力内容に不備があるため更新できませんでした。入力内容をご確認ください。'
      render :show
    end
  end

  private

  def user_params
    params.require(:user).permit(:bank_name, :bank_code, :bank_branch_name,
                                 :bank_branch_number, :bank_account_type,
                                 :bank_account_number, :bank_account_holder)
  end

end
