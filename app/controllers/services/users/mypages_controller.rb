class Services::Users::MypagesController < ApplicationController
  def show
    redirect_to service_path(identifier: @service.identifier) and return if current_user.blank?
    @user = current_user
  end

  def update
    @user = current_user
    is_change_email = @user.email != user_params[:email]
    if @user.update(user_params)
      if is_change_email
        message = 'メールアドレスが変更されました。ログイン時に入力するメールアドレスも変更されましたのでご注意ください。'
      else
        message = 'ご登録情報を更新しました。'
      end
      redirect_to service_users_mypage_path, notice: message
    else
      flash.now[:error] = '入力内容に不備があるため更新できませんでした。入力内容をご確認ください。'
      render :show
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :profile_image)
  end

end
