class Services::Users::PaymentsController < ApplicationController

  def show
    # 未ログインまたは課金設定がない場合リダイレクト
    if current_user.blank? || (!@service.transaction_payment? && !@service.subscription_payment?)
      redirect_to service_path(identifier: @service.identifier) and return
    end

    @user = current_user
    if @user.pay_jp_customer_id.present?
      pay_jp_customer = Payjp::Customer.retrieve(@user.pay_jp_customer_id)
      @pay_jp_card = pay_jp_customer.cards.all.first
    end
  end

  def pay
    # redirect_to root_path, alert: '支払金額が不正です。' and return if params[:pay_amount].blank? || params[:pay_amount].to_i < 50

    post_entry = PostEntry.find_by(identifier: params[:post_entry_identifier])
    redirect_to root_path, alert: '支払に失敗しました。' and return if post_entry.blank?

    redirect_to service_users_payment_path, alert: 'クレジットカード情報を登録してから支払ってください。' and return if current_user.pay_jp_customer_id.blank?

    begin
      customer = Payjp::Customer.retrieve(current_user.pay_jp_customer_id)
      card = customer.cards.retrieve(customer.cards.all().data.first.id)
      charge = Payjp::Charge.create(
        amount: post_entry.price,
        customer: customer.id,
        card: card.id,
        currency: 'jpy'
      )
      PaymentHistory.create(
        post_entry_id: post_entry.id,
        user_id: current_user.id,
        pay_jp_charge_id: charge.id
      )
      post_entry.update(status: PostEntry.statuses[:paid])
      redirect_to service_post_entry_path(service_identifier: post_entry.post.service_identifier, identifier: post_entry.identifier), notice: '支払い処理が完了しました。'
    rescue
      redirect_to service_post_entry_path(service_identifier: post_entry.post.service_identifier, identifier: post_entry.identifier), alert: '支払いに失敗しました。'
    end
  end

  def register_credit_card
    pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
    card = pay_jp_token.card
    fingerprint = card.fingerprint

    customer = Payjp::Customer.create(card: pay_jp_token.id)
    redirect_to root_path, error: 'クレジットカード情報の登録に失敗しました' and return if customer[:error].present?

    if current_user.update(pay_jp_customer_id: customer.id, pay_jp_fingerprint: fingerprint)
      redirect_to service_users_payment_path, notice: 'クレジットカード情報を登録しました。'
    else
      redirect_to root_path, error: 'クレジットカード情報の登録に失敗しました'
    end
  end

  def change_credit_card
    customer = Payjp::Customer.retrieve(current_user.pay_jp_customer_id)

    pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
    card = pay_jp_token.card
    fingerprint = card.fingerprint

    # クレジットカード削除
    card = customer.cards.retrieve(customer.cards.all().data.first.id)
    card.delete
    customer.cards.create(
      card: pay_jp_token.id
    )

    current_user.update(pay_jp_fingerprint: fingerprint)

    redirect_to service_users_payment_path, notice: 'お支払い情報を変更しました。'
  end

  def cancel_register_credit_card
    customer = Payjp::Customer.retrieve(current_user.pay_jp_customer_id)
    card = customer.cards.retrieve(customer.cards.all().data.first.id)
    card.delete
    customer.delete
    current_user.update(pay_jp_customer_id: nil, pay_jp_fingerprint: nil)
    redirect_to root_path and return
  end

  def register_plan
    # 定期課金の登録
    if current_user.pay_jp_customer_id.blank?
      pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
      card = pay_jp_token.card
      fingerprint = card.fingerprint

      customer = Payjp::Customer.create(card: pay_jp_token.id)
      redirect_to root_path, error: 'クレジットカード情報の登録に失敗しました' and return if customer[:error].present?

      unless current_user.update(pay_jp_customer_id: customer.id, pay_jp_fingerprint: fingerprint)
        redirect_to root_path, error: 'クレジットカード情報の登録に失敗しました' and return if customer[:error].present?
      end
    end

    subscription = Payjp::Subscription.create(customer: current_user.pay_jp_customer_id, plan: @service.pay_jp_plan_id)
    if subscription[:error].present?
      redirect_to service_users_plan_path, alert: 'プランの登録に失敗しました'
    else
      if current_user.update(pay_jp_subscription_id: subscription.id)
        redirect_to service_users_plan_path, notice: 'プランに登録しました。'
      else
        redirect_to service_users_plan_path, error: 'プランの登録に失敗しました'
      end
    end
  end

  def cancel_plan
    # 定期課金の解約
    if current_user.delete_subscription
      redirect_to service_users_plan_path, notice: 'プランを解約しました。'
    else
      redirect_to service_users_plan_path, error: 'プランの解約に失敗しました'
    end
  end

end
