class Services::Users::PlansController < ApplicationController
  def show
    # 未ログインまたは月額課金設定がない場合リダイレクト
    if current_user.blank? || !@service.subscription_payment?
      redirect_to root_path and return
    end
    @user = current_user
    if @user.pay_jp_customer_id.present?
      pay_jp_customer = Payjp::Customer.retrieve(@user.pay_jp_customer_id)
      @pay_jp_card = pay_jp_customer.cards.all.first
    end
    if current_user.pay_jp_subscription_id.present?
      @pay_jp_subscription = Payjp::Subscription.retrieve(current_user.pay_jp_subscription_id)
    end
  end
end
