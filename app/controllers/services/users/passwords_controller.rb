# frozen_string_literal: true

class Services::Users::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  # protected

  def after_resetting_password_path_for(resource)
    service_path(resource.service.identifier)
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    service_path(@service.identifier)
  end

  def assert_reset_token_passed
    if params[:reset_password_token].blank?
      set_flash_message(:alert, :no_token)
      redirect_to new_user_session_path(service_identifier: @service.identifier)
    end
  end
end
