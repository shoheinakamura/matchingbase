class Services::Users::UnsubscribesController < ApplicationController
  def create
    if params[:service_identifier].present?
      res = current_user.unsubscribe(unsubscribe_reason: params[:unsubscribe_reason])
      if res[:result]
        service_identifier = current_user.service_identifier
        sign_out(current_user)
        redirect_to service_path(service_identifier), notice: res[:message]
      else
        redirect_to service_path(current_user.service_identifier), alert: res[:message]
      end
    else
      redirect_to service_path(current_user.service_identifier), alert: '退会処理でエラーが発生しました。お問い合わせフォームよりお問い合わせください。エラーコード01'
    end
  end
end
