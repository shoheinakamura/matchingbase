class ManageServiceReleasesController < ProvidersController

  def edit
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service_release

    if service_params[:status].blank?
      flash[:error] = '公開設定を選択してください'
      render :edit and return
    end

    # 募集アクション名と応募アクション名の入力チェック
    if @service.post_action.blank? || @service.apply_action.blank?
      flash[:error] = 'マッチング設定の募集アクション名と応募アクション名が設定されていないため変更できません。'
      render :edit and return
    end

    if @service.update(service_params)
      # 非公開にする場合はuserのクレジットカード情報を削除する
      @service.reset_users_payment_info if @service.pending?

      flash[:success] = '更新しました'
      if ActiveRecord::Type::Boolean.new.cast(params[:from_new])
        redirect_to manage_services_path
      else
        redirect_to edit_manage_service_release_path(identifier: @service.identifier)
      end
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  private

  def service_params
    params.require(:service).permit(
      :status
    )
  end

end
