class ManageServiceAdminsController < ProvidersController

  def edit
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service_admin

    if @service.update(service_params)
      flash[:success] = '更新しました'
      if ActiveRecord::Type::Boolean.new.cast(params[:from_new])
        redirect_to edit_manage_service_lp_path(identifier: @service.identifier, from_new: params[:from_new])
      else
        redirect_to edit_manage_service_admin_path(identifier: @service.identifier)
      end
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  private

  def service_params
    params.require(:service).permit(
      :admin_enable,
      :admin_type,
      :admin_name,
      :admin_contact,
      :admin_url,
      :terms_enable,
      :terms_content,
      :privacy_policy_enable,
      :privacy_policy_content,
      :commercial_transaction_enable,
      :commercial_transaction_content,
      :contact_form_enable,
      :contact_form_mail,
      :mail_signature
    )
  end

end
