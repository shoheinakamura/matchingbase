class Providers::UnsubscribesController < ProvidersController
  def create

    unsubscribe_email = current_provider.email << '.unsubscribe' << Time.now.strftime('%Y%m%d%H%M%S')
    if current_provider.pay_jp_customer_id.present?
      # クレジットカード削除
      CreditCardArchive.create(
        provider_id: current_provider.id,
        pay_jp_fingerprint: current_provider.pay_jp_fingerprint,
        reason: CreditCardArchive.reasons[:unsubscribe]
      )
      customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
      card = customer.cards.retrieve(customer.cards.all().data.first.id)
      card.delete
      customer.delete
    end

    if Provider.find_by(email: unsubscribe_email).blank?
      if current_provider.update(email: unsubscribe_email, unsubscribe_reason: params[:unsubscribe_reason], pay_jp_fingerprint: nil)
        # 作成済みサービス削除
        current_provider.destroy_all_service
        # ログアウト
        sign_out(current_provider)
        redirect_to new_provider_session_path, notice: '退会しました。ご利用いただきましてありがとうございました。（登録情報を削除しました。）'
      else
        redirect_to new_provider_session_path, alert: '退会処理でエラーが発生しました。お問い合わせフォームよりお問い合わせください。エラーコード03'
      end
    else
      redirect_to new_provider_session_path, alert: '退会処理でエラーが発生しました。お問い合わせフォームよりお問い合わせください。エラーコード02'
    end
  end
end
