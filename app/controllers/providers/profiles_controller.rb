class Providers::ProfilesController < ProvidersController
  before_action :require_login

  def show
    if current_provider.pay_jp_customer_id.present?
      pay_jp_customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
      @pay_jp_card = pay_jp_customer.cards.all.first
    end
    if current_provider.pay_jp_subscription_id.present?
      @pay_jp_subscription = Payjp::Subscription.retrieve(current_provider.pay_jp_subscription_id)
    end
  end

  def update
    if current_provider.update(provider_params)
      redirect_to providers_profile_path, notice: 'ご登録情報を更新しました。'
    else
      render :edit
    end
  end

  private

  def provider_params
    params.require(:provider).permit(
      :email
    )
  end
end
