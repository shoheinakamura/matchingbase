class ContactsController < ApplicationController
  layout 'provider_contact_application'

  def show
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      ContactMailer.contact_mail(@contact).deliver
      ContactMailer.provider_notification_mail(@contact).deliver
      flash[:success] = 'お問い合わせを受け付けました。'
      redirect_to contact_path
    else
      render :show
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :message)
  end
end
