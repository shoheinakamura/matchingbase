class ManageServiceMatchingsController < ProvidersController

  def edit
    @service = Service.find_by(identifier: params[:identifier])
    @post_template = @service.post_templates&.first

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service_matching

    # 募集アクション名と応募アクション名の入力チェック
    if service_params[:post_action].blank? || service_params[:apply_action].blank?
      flash[:error] = '募集アクション名と応募アクション名は必須です'
      render :edit and return
    end

    # カテゴリー修正
    if params[:category].present?
      update_category_ids = []
      if params[:categories].present?
        params[:categories].permit!.to_h.each do |exist_category|
          next if exist_category[1].blank?
          category = Category.find_by(id: exist_category[0], service_id: @service.id)
          category.update(name: exist_category[1]) if category.present?
          update_category_ids.push(category.id)
        end
      end
      # カテゴリー削除
      delete_categories = Category.where(service_id: @service.id).where.not(id: update_category_ids)
      delete_categories.destroy_all

      # 新規カテゴリー追加
      if params[:new_categories].present?
        params[:new_categories].each do |new_category|
          next if new_category.blank?
          Category.create(name: new_category, service_id: @service.id)
        end
      end
    end

    # タグ修正
    if params[:tag].present?
      update_tag_ids = []
      if params[:tags].present?
        params[:tags].permit!.to_h.each do |exist_tag|
          next if exist_tag[1].blank?
          tag = Tag.find_by(id: exist_tag[0], service_id: @service.id)
          tag.update(name: exist_tag[1]) if tag.present?
          update_tag_ids.push(tag.id)
        end
      end
      # タグ削除
      delete_tags = Tag.where(service_id: @service.id).where.not(id: update_tag_ids)
      delete_tags.destroy_all

      # 新規タグ追加
      if params[:new_tags].present?
        params[:new_tags].each do |new_tag|
          next if new_tag.blank?
          Tag.create(name: new_tag, service_id: @service.id)
        end
      end
    end

    # post_template
    if params[:post_template].present?
      post_template = @service.post_templates&.first
      if post_template.blank?
        PostTemplate.create(
          service_identifier: @service.identifier,
          content: params[:post_template]
        )
      elsif post_template.content != params[:post_template]
        post_template.update(content: params[:post_template])
      end
    end

    if @service.update(service_params)
      flash[:success] = '更新しました'
      if ActiveRecord::Type::Boolean.new.cast(params[:from_new])
        redirect_to edit_manage_service_price_path(identifier: @service.identifier, from_new: params[:from_new])
      else
        redirect_to edit_manage_service_matching_path(identifier: @service.identifier)
      end
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  private

  def service_params
    params.require(:service).permit(
      :post_action,
      :apply_action
    )
  end

end
