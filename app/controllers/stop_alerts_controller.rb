class StopAlertsController < ApplicationController
  def show
    redirect_to service_path(@service.identifier) unless @service.stoped?
  end
end
