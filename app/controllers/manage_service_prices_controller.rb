class ManageServicePricesController < ProvidersController

  def edit
    @service = Service.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'サービス一覧', path: manage_services_path},
      {display_text: @service.name, path: manage_service_path(@service.identifier)},
      {display_text: "#{@service.name}の編集"}
    )
  end

  def update
    @service = Service.find_by(identifier: params[:identifier])
    @flow_point = :manage_service_price

    # 募集アクションがある場合はアクション課金設定の変更は不可
    if @service.posts.present? &&
      (
        @service.transaction_payment != ActiveRecord::Type::Boolean.new.cast(service_params[:transaction_payment]) ||
          @service.paid_by_post_user? && service_params[:transaction_payment_type] != 'paid_by_post_user' ||
          @service.paid_by_apply_user? && service_params[:transaction_payment_type] != 'paid_by_apply_user'
      )
      flash[:error] = 'すでに募集アクションがあるためアクション有料化設定の変更は不可です'
      render :edit and return
    end

    # アクション有料化
    if ActiveRecord::Type::Boolean.new.cast(service_params[:transaction_payment])
      if service_params[:pay_jp_private_key].blank?
        flash[:error] = 'ユーザー同士の支払いを有効にする場合「PAY.JP 本番秘密鍵」が必須です'
        render :edit and return
      end
      if service_params[:pay_jp_public_key].blank?
        flash[:error] = 'ユーザー同士の支払いを有効にする場合「PAY.JP 本番公開鍵」が必須です'
        render :edit and return
      end
      if service_params[:transaction_payment_margin].blank?
        flash[:error] = 'ユーザー同士の支払いを有効にする場合「手数料率」が必須です'
        render :edit and return
      end
    end

    # 定額
    # if ActiveRecord::Type::Boolean.new.cast(service_params[:transaction_payment_flat])
    #   if service_params[:transaction_payment_price].blank?
    #     flash[:error] = '定額価格を設定してください'
    #     render :edit and return
    #   end
    # end

    # 月額有料化
    if ActiveRecord::Type::Boolean.new.cast(service_params[:subscription_payment])
      if service_params[:pay_jp_private_key].blank?
        flash[:error] = '月額を有料化する場合「PAY.JP 本番秘密鍵」が必須です'
        render :edit and return
      end
      if service_params[:pay_jp_public_key].blank?
        flash[:error] = '月額を有料化する場合「PAY.JP 本番公開鍵」が必須です'
        render :edit and return
      end
      if service_params[:pay_jp_plan_id].blank?
        flash[:error] = '月額を有料化する場合「PAY.JP プランID」が必須です'
        render :edit and return
      end
    end

    if service_params[:pay_jp_private_key].present?
      unless service_params[:pay_jp_private_key].start_with?('sk_')
        flash[:error] = 'PAY.JP 本番秘密鍵は「sk_live_」からはじまる情報を保存してください'
        render :edit and return
      end

      # PAY.JP疎通チェック
      Payjp.api_key = service_params[:pay_jp_private_key]
      begin
        Payjp::Account.retrieve
      rescue => e
        flash[:error] = 'PAY.JP 本番秘密鍵が正しくありません。'
        render :edit and return
      end
    end
    if service_params[:pay_jp_public_key].present? && !service_params[:pay_jp_public_key].start_with?('pk_')
      flash[:error] = 'PAY.JP 本番公開鍵は「pk_live_」からはじまる情報を保存してください'
      render :edit and return
    end

    # プランIDチェック
    if service_params[:pay_jp_private_key].present? && service_params[:pay_jp_plan_id].present?
      # PAY.JP疎通チェック
      Payjp.api_key = service_params[:pay_jp_private_key]
      begin
        unless Payjp::Plan.all[:data].map{|plan| plan.id}.include?(service_params[:pay_jp_plan_id])
          flash[:error] = 'PAY.JP プランIDが正しくありません。'
          render :edit and return
        end
      rescue => e
        flash[:error] = 'PAY.JP プラン情報の取得に失敗しました。'
        render :edit and return
      end
    end

    if @service.update(service_params)
      # 月額有料化OFFの場合ユーザーのサブスクリプションを解約する
      @service.delete_users_subscription unless @service.subscription_payment

      flash[:success] = '更新しました'
      if ActiveRecord::Type::Boolean.new.cast(params[:from_new])
        redirect_to edit_manage_service_admin_path(identifier: @service.identifier, from_new: params[:from_new])
      else
        redirect_to edit_manage_service_price_path(identifier: @service.identifier)
      end
    else
      flash.now[:alert] = create_error_message(@service.errors.messages)
      render :edit
    end
  end

  private

  def service_params
    params.require(:service).permit(
      :transaction_payment,
      :transaction_payment_type,
      :transaction_payment_flat,
      :transaction_payment_price,
      :transaction_payment_margin,
      :subscription_payment,
      :subscription_payment_limit_type,
      :pay_jp_public_key,
      :pay_jp_private_key,
      :pay_jp_plan_id
    )
  end

end
