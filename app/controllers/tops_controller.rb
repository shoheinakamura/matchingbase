class TopsController < ProvidersController
  def index
    @services = current_provider.services.order(updated_at: :DESC).page(params[:page]).per(10)

    @from_provider_profile = params[:from_provider_profile]
  end

  def pay
    pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
    card = pay_jp_token.card
    fingerprint = card.fingerprint

    customer = Payjp::Customer.create(card: pay_jp_token.id)
    redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました。別のクレジットカードをお試しください。'} and return if customer[:error].present?

    begin
      subscription = Payjp::Subscription.create(customer:customer.id, plan: current_provider.campaign_plan_id.present? ? current_provider.campaign_plan_id : Rails.env.production? ? 'trial_30_cp_1_plan' : 'test_plan_002')
      redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました。別のクレジットカードをお試しください。'} and return if subscription[:error].present?
    rescue => e
      redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました。別のクレジットカードをお試しください。'} and return
    end

    if current_provider.update(pay_jp_customer_id: customer.id, pay_jp_subscription_id: subscription.id, pay_jp_fingerprint: fingerprint)
      redirect_to root_path, notice: 'ご登録が完了しました！マッチングベースをご利用ください！' and return
    else
      redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました。別のクレジットカードをお試しください。'} and return
    end
  end

  def register_credit_card
    pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
    card = pay_jp_token.card
    fingerprint = card.fingerprint

    customer = Payjp::Customer.create(card: pay_jp_token.id)
    redirect_to providers_profile_path, error: 'クレジットカード情報の登録に失敗しました' and return if customer[:error].present?

    if current_provider.pay_jp_subscription_id.blank? && current_provider.trial_expired_at < Time.now
      # トライアルが終了しているにも関わらず定期課金が開始されていない場合は開始する
      customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
      Payjp::Subscription.create(customer:customer.id, plan: current_provider.campaign_plan_id.present? ? current_provider.campaign_plan_id : Rails.env.production? ? 'trial_30_cp_1_plan' : 'test_plan_002')
    end

    if current_provider.update(pay_jp_customer_id: customer.id, pay_jp_fingerprint: fingerprint)
      redirect_to providers_profile_path, notice: 'クレジットカード情報を登録しました。'
    else
      redirect_to providers_profile_path, error: 'クレジットカード情報の登録に失敗しました'
    end
  end

  def continue_register_credit_card
    customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
    card = customer.cards.retrieve(customer.cards.all().data.first.id)
    begin
      subscription = Payjp::Subscription.create(customer:customer.id, plan: current_provider.plan_id.present? ? current_provider.plan_id : Rails.env.production? ? 'no_trial_cp_1_plan' : 'no_trial_test_plan')
    rescue => e
      Rails.logger.info(e)
      redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました'} and return
    end
    fingerprint = card.fingerprint
    if current_provider.update(pay_jp_customer_id: customer.id, pay_jp_subscription_id: subscription.id, pay_jp_fingerprint: fingerprint)
      redirect_to root_path, notice: 'ご登録が完了しました！マッチングベースをご利用ください！' and return
    else
      redirect_to root_path, flash: {exist_credit_card_error: true, exist_credit_card_error_message: 'クレジットカード情報の登録に失敗しました'} and return
    end
  end

  def cancel_register_credit_card
    customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
    card = customer.cards.retrieve(customer.cards.all().data.first.id)
    card.delete
    customer.delete
    current_provider.update(pay_jp_customer_id: nil, pay_jp_fingerprint: nil)
    redirect_to root_path and return
  end

  def change_credit_card
    customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)

    pay_jp_token = Payjp::Token.retrieve(params['payjp-token'])
    card = pay_jp_token.card
    fingerprint = card.fingerprint

    # クレジットカード削除
    CreditCardArchive.create(
      provider_id: current_provider.id,
      pay_jp_fingerprint: current_provider.pay_jp_fingerprint,
      reason: CreditCardArchive.reasons[:change_credit_card]
    )
    card = customer.cards.retrieve(customer.cards.all().data.first.id)
    card.delete
    customer.cards.create(
      card: pay_jp_token.id
    )

    current_provider.update(pay_jp_fingerprint: fingerprint)

    if current_provider.pay_jp_subscription_id.present?
      # 定期課金が停止している場合は再開する
      subscription = Payjp::Subscription.retrieve(current_provider.pay_jp_subscription_id)
      subscription.resume if subscription.status == 'paused'
    elsif current_provider.trial_expired_at < Time.now
      # トライアルが終了しているにも関わらず定期課金が開始されていない場合は開始する
      customer = Payjp::Customer.retrieve(current_provider.pay_jp_customer_id)
      Payjp::Subscription.create(customer:customer.id, plan: current_provider.campaign_plan_id.present? ? current_provider.campaign_plan_id : Rails.env.production? ? 'trial_30_cp_1_plan' : 'test_plan_002')
    end

    redirect_to providers_profile_path, notice: 'お支払い情報を変更しました。' and return
  end
end
