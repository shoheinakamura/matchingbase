class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_service,:validate_service_identifier, if: :exist_service_identifier_params?
  before_action :force_logout

  def exist_service_identifier_params?
    params[:identifier].present? || params[:service_identifier].present?
  end

  def after_sign_in_path_for(resource)
    if resource.model_name.name == 'Provider'
      root_path
    else
      service_path(resource.service_identifier)
    end
  end

  def after_sign_out_path_for(resource)
    if resource == :provider
      root_path
    else
      service_path(@service.identifier)
    end
  end

  def require_login
    if self._prefixes.include?('application')
      redirect_to new_user_session_path(service_identifier: @service.identifier), alert: 'ログインしてください' unless user_signed_in?
    end
  end

  def validate_service_identifier
    sign_out if user_signed_in? && @service.present? && current_user.service_identifier != @service.identifier
  end

  def configure_permitted_parameters
    added_attrs = [ :service_identifier, :name, :email, :password, :password_confirmation　]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
  end

  def set_service
    @service = if controller_name == 'services'
                 Service.find_by(identifier: params[:identifier])
               else
                 Service.find_by(identifier: params[:service_identifier])
               end
    # payjpセット
    Payjp.api_key = @service.pay_jp_private_key if @service.present?

    if @service.blank? || @service.pending?
      redirect_to root_path
    elsif controller_name != 'stop_alerts' &&
      controller_name != 'sessions' &&
      controller_name != 'contacts' &&
      controller_name != 'mypages' &&
      controller_name != 'unsubscribes' &&
      controller_name != 'banks' &&
      controller_name != 'payments' &&
      controller_name != 'plans' &&
      @service.stoped?
      redirect_to service_stop_alert_path(@service.identifier)
    end
  end

  def force_logout
    if current_user.present? && current_user.unsubscribed?
      logout
      redirect_to new_user_session_path(service_identifier: @service.identifier)
    end
  end

  def create_error_message(error_messages)
    errors = ''
    error_messages.each_with_index do |message, index|
      errors << "#{message[1].present? ? message[1][0] : message}#{'<br>' if index != error_messages.size - 1}"
    end
    errors.html_safe
  end
end
