class ProvidersController < ProviderApplicationController
  layout 'provider_application'

  before_action :check_payment

  private

  def check_payment
    # 登録から30日後のチェック
    if current_provider.trial_expired_at < Time.now
      @not_exist_subscription = current_provider.pay_jp_customer_id.blank? || current_provider.pay_jp_subscription_id.blank?

      # 支払状況の確認
      if current_provider.pay_jp_subscription_id.present?
        subscription = Payjp::Subscription.retrieve(current_provider.pay_jp_subscription_id)
        @paused_payment = subscription.blank? || subscription.status == 'paused'
      end
    end
  end
end
