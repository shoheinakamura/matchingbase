class ManageUsersController < ProvidersController

  def index
    if params[:service_identifier].present?
      @service = current_provider.services.find_by(identifier: params[:service_identifier])
    else
      @service = current_provider.services.first
    end
    if @service.present?
      @users = @service.users.where(unsubscribe_at: nil)

      if params[:name].present?
        @users = @users.where('name LIKE ?',"%#{params[:name]}%")
      end
      if params[:email].present?
        @users = @users.where('email LIKE ?',"%#{params[:email]}%")
      end
    end
  end

  def show
    @user = User.find_by(identifier: params[:identifier])

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'ユーザー一覧', path: manage_users_path(service_identifier: @user.service.identifier)},
      {display_text: @user.name}
    )
  end

  def destroy
    @user = User.find_by(identifier: params[:identifier])
    res = @user.unsubscribe(unsubscribe_reason: params[:unsubscribe_reason])

    if res[:result]
      redirect_to manage_users_path, notice: '退会処理が完了しました。'
    else
      flash[:alert] = res[:message]
      render :index
    end
  end

  def chat_room
    @user = User.find_by(identifier: params[:identifier])
    redirect_to manage_user_path(@user.identifier), notice: 'チャットはありません。' and return if @user.chat_members.blank?

    @breadcrumbs = []
    @breadcrumbs.push(
      {display_text: 'ユーザー一覧', path: manage_users_path(service_identifier: @user.service.identifier)},
      {display_text: @user.name, path: manage_user_path(@user.identifier)},
      {display_text: 'チャット'}
    )

    chat_room_ids = @user.chat_members.pluck(:chat_room_id).uniq
    @chat_rooms = ChatRoom.includes([chat_members: :user], :chat_messages).where(id: chat_room_ids).order(updated_at: :desc)
    @chat_room = if params[:chat_room_identifier].present?
                   ChatRoom.find_by(identifier: params[:chat_room_identifier])
                 else
                   @chat_rooms.first
                 end
    @chat_messages = @chat_room.chat_messages
  end

end
