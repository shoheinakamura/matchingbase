class ChatChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chat_room_#{params['chat_room_identifier']}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    chat_room = ChatRoom.find_by(identifier: data['chat_room_identifier'])

    # upload_file_uri = data['upload_file_uri']
    # if upload_file_uri.present?
    #   base64_string = upload_file_uri.split(';base64,')[1]
    #   blob = Base64.decode64(base64_string)
    #   image = MiniMagick::Image.read(blob)
    # end

    ChatMessage.create(
      content: data['content'],
      # upload_file: image,
      chat_room_id: chat_room.id,
      # message_type: image.present? ? ChatMessage.message_types[:file_message] : ChatMessage.message_types[:text_message],
      message_type: ChatMessage.message_types[:text_message],
      user_id: current_user.id
    )
  end
end
