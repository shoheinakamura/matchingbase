$(function () {
    $('#js-status').change(function () {
        if ($(this).attr('is_released') == 'true' && $(this).val() == 'pending') {
            $('#js-submit').attr('data-confirm', '非公開にするとサービスに登録中のユーザーのクレジットカード情報と月額課金情報が削除されます。変更しますか？');
        } else {
            $('#js-submit').removeAttr('data-confirm');
        }
    })
});