$(function () {
    $('#js-admin_type').on('change', function () {
        if ($(this).val() == 'internal_link') {
            $('#js-admin_name').attr('disabled', false);
            $('#js-admin_contact').attr('disabled', false);
            $('#js-admin_url').attr('disabled', true);
        } else {
            $('#js-admin_name').attr('disabled', true);
            $('#js-admin_contact').attr('disabled', true);
            $('#js-admin_url').attr('disabled', false);
        }
    });
});