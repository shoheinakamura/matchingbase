$(function () {
    addEventDeleteCategory();
    addEventDeleteTag();

    $('#js-add-category-button').on('click', function () {
        var $this = $(this);
        $this.before(
            '<div class="category-input-group">\n' +
            '<div class="mb-3 d-flex">' +
            '<input type="text" name="new_categories[]" class="form-control mr-2">\n' +
            '<button class="btn btn-danger js-delete-category-button" type="button">☓</button>' +
            '</div>' +
            '</div>'
        );
        addEventDeleteCategory();
    });

    function addEventDeleteCategory() {
        $('.js-delete-category-button').off('click');
        $('.js-delete-category-button').on('click', function () {
            $(this).closest('.category-input-group').remove();
        });
    }

    $('#js-add-tag-button').on('click', function () {
        var $this = $(this);
        $this.before(
            '<div class="tag-input-group">\n' +
            '<div class="mb-3 d-flex">' +
            '<input type="text" name="new_tags[]" class="form-control mr-2">\n' +
            '<button class="btn btn-danger js-delete-tag-button" type="button">☓</button>' +
            '</div>' +
            '</div>'
        );
        addEventDeleteTag();
    });

    function addEventDeleteTag() {
        $('.js-delete-tag-button').off('click');
        $('.js-delete-tag-button').on('click', function () {
            $(this).closest('.tag-input-group').remove();
        });
    }
});