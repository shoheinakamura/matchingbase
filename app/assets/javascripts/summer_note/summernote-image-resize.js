(function (factory) {
    /* Global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {
    /**
     * @class plugin.videoUpload
     *
     * video upload plugin
     */
    $.extend(true, $.summernote.lang, {
        'ja-JP': {
            image3Resize: {
                dialogTitle: '1/3'
            }
        }
    });

    $.extend($.summernote.options, {
        image3Resize: {
            icon: '<i class="note-icon-picture">1/3</i>'
        }
    });

    $.extend($.summernote.plugins, {
        'image3Resize': function (context) {
            var self      = this,
                ui        = $.summernote.ui,
                $note     = context.layoutInfo.note,
                $editor   = context.layoutInfo.editor,
                $editable = context.layoutInfo.editable,
                options   = context.options,
                lang      = options.langInfo;
            context.memo('button.image3Resize', function() {
                var button = ui.button({
                    contents: options.image3Resize.icon,
                    container: "body",
                    click: function () {
                        context.invoke('image3Resize.show');
                    }
                });
                return button.render();
            });
            this.initialize = function () {
            };
            this.show = function () {
                var $img    = $($editable.data('target'));
                // var imgInfo = {
                //     imgDom:  $img,
                //     title:   $img.attr('title'),
                //     src:     $img.attr('src'),
                //     alt:     $img.attr('alt'),
                //     width:   $img.attr('width'),
                //     height:  $img.attr('height'),
                //     role:    $img.attr('role'),
                //     class:   $img.attr('class'),
                //     style:   $img.attr('style'),
                //     imgLink: $($img).parent().is("a") ? $($img).parent() : null
                // };
                $img.attr('style', 'width: 33.333333%;');
                $('.note-popover').css("display", "none");
                $('.note-control-selection').css("display", "none");
            };
        }
    });
}));
