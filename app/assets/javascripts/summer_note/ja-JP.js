$.extend(true,$.summernote.lang,{
  'ja-JP':{ /* Korean */
    imageAttributes:{
      dialogTitle: '写真設定',
      tooltip: '写真設定',
      tabImage: '写真',
        src: 'パス',
        browse: 'ナビゲーション',
        title: 'タイトル',
        alt: '説明',
        dimensions: 'サイズ',
      tabAttributes: '属性',
        class: 'クラス',
        style: 'スタイル',
        role: '役割',
      tabLink: 'リンク',
        linkHref: 'URL',
        linkTarget: '開く方法',
        linkTargetInfo: 'オプション: _self, _blank, _top, _parent',
        linkClass: 'クラス',
        linkStyle: 'スタイル',
        linkRel: '関係',
        linkRelInfo: 'オプション: alternate, author, bookmark, help, license, next, nofollow, noreferrer, prefetch, prev, search, tag',
        linkRole: '役割',
      tabUpload: 'アップロード',
        upload: 'アップロード',
      tabBrowse: 'ナビゲーション',
      editBtn: '編集'
    }
  }
});
