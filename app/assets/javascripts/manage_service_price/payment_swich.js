$(function () {
    var $transactionPayment = $('#transaction_payment');
    var $subscriptionPayment = $('#subscription_payment');

    $transactionPayment.on('change', function () {
        if ($subscriptionPayment[0].checked) {
            return;
        }

        var $this = $(this);
        if ($this[0].checked) {
            $('#js-pay_jp_private_key').removeClass('d-none');
            $('#js-pay_jp_public_key').removeClass('d-none');
        } else {
            $('#js-pay_jp_private_key').addClass('d-none');
            $('#js-pay_jp_public_key').addClass('d-none');
        }
    });

    $subscriptionPayment.on('change', function () {
        var $this = $(this);
        if ($this[0].checked) {
            $('#js-pay_jp_private_key').removeClass('d-none');
            $('#js-pay_jp_public_key').removeClass('d-none');
            $('#js-pay_jp_plan_id').removeClass('d-none');
        } else {
            if (!$transactionPayment[0].checked) {
                $('#js-pay_jp_private_key').addClass('d-none');
                $('#js-pay_jp_public_key').addClass('d-none');
            }
            $('#js-pay_jp_plan_id').addClass('d-none');
        }
    });
});