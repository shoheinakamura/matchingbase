$(function () {
    var chatUtil = new ChatUtil();

    var $roomSpeaker = $('#room_speaker');
    var $uploadFile = $('#upload_file');

    $('#send-text-button').on('click', function () {
        var content = $.trim($roomSpeaker.val());
        App.chat.speak(content);
        $roomSpeaker.val('');
    });

    $('#send-file-button').on('click', function () {
        if ($uploadFile.size() > 0 && $uploadFile.get(0).files.length > 0) {
            sendFile($uploadFile.get(0).files[0]);
            $uploadFile.val('');
        }
    });

    // function sentMessage() {
    //     // ファイルの場合はajaxでデータ作成
    //     if ($uploadFile.size() > 0 && $uploadFile.get(0).files.length > 0) {
    //         sendFile($uploadFile.get(0).files[0]);
    //     } else {
    //         var content = $.trim($roomSpeaker.val());
    //         App.chat.speak(content);
    //     }
    //
    //     $roomSpeaker.val('');
    //     $uploadFile.val('');
    // }

    $('#chat_messages').css('height', $(window).height()-200);
    $('#chat_rooms').css('height', $(window).height()-200);
    chatUtil.scrollBottom();

    function sendFile(upload_file) {
        data = new FormData();
        data.append('upload_file', upload_file);
        data.append('chat_room_identifier', $('#chat_messages').data('chat_room_identifier'));
        var service_identifier = $('#chat_messages').data('service_identifier');
        $.ajax({
            url: '/services/' + service_identifier + '/chat_room/upload_file',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                if (data.res) {
                } else {
                    alert('ファイルの送信に失敗しました');
                }
            },
            error: function () {
                alert('ファイルの送信に失敗しました');
            }
        });
    }
});