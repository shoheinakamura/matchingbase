$(function () {
    var $jsInputSwitchCheck = $('.js-input-switch-check');

    $jsInputSwitchCheck.on('change', function () {
        var $this = $(this);
        var $jsInputSwitchHidden = $($($this.parents('.js-input-switch')[0]).find('.js-input-switch-hidden')[0]);
        if ($this[0].checked) {
            $jsInputSwitchHidden.slideDown('fast');
        } else {
            $jsInputSwitchHidden.slideUp('fast');
        }
    });

    $jsInputSwitchCheck.each( function( index, element ){
        var $jsInputSwitchHidden = $($($(element).parents('.js-input-switch')[0]).find('.js-input-switch-hidden')[0]);
        if ($(element)[0].checked) {
            $jsInputSwitchHidden.css('display', 'block');
        } else {
            $jsInputSwitchHidden.css('display', 'none');
        }
    });
});