$(function () {
    var mainColor = $('#main_color').val();

    if (mainColor == '#ffffff' || mainColor == '') {
        $('.common-emphasis-button').css(
            {
                background: '#444',
                'border-color': '#444'
            }
        );
        $('.common-normal-button').css(
            {
                color: '#444',
                background: mainColor,
                'border-color': '#444'
            }
        );
        $('.common-checkbox__after').css(
            {
                'border-right': '3px solid #444',
                'border-bottom': '3px solid #444'
            }
        )
    } else {
        $('.common-emphasis-button').css(
            {
                background: mainColor,
                'border-color': mainColor
            }
        );
        $('.common-normal-button').css(
            {
                color: mainColor,
                'border-color': mainColor
            }
        );
        $('.common-emphasis-label').css(
            {
                background: mainColor,
                'border-color': mainColor
            }
        );
        $('.common-normal-label').css(
            {
                background: '#fff',
                'border': 'solid 1px ' + mainColor,
                color: mainColor,
            }
        );
        $('.common-checkbox__after').css(
            {
                'border-right': '3px solid ' + mainColor,
                'border-bottom': '3px solid ' + mainColor
            }
        )
    }
});