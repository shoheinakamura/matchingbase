$(function () {

    $('.input-check').on('input', function () {
        checkForm(this)
    });

    function checkForm($this)
    {
        var str=$this.value;
        while(str.match(/[^A-Z^a-z\d\-\_]/))
        {
            str=str.replace(/[^A-Z^a-z\d\-\_]/,"");
        }
        $this.value=str;
    }
});