$(function () {
    $('#js-insert-post-template').on('click', function () {
        var service_identifier = $(this).data('service_identifier');
        $.ajax({
            url: '/services/' + service_identifier + '/posts/insert_post_template',
            data: {id: 1},
            type: 'POST',
            success: function(data) {
                if ($('.note-codable').is(':visible')) {
                    alert('コードビューの場合は挿入できません。');
                    return;
                }
                $('.note-editable').append(data.content);
                $('.common-rich-editor').val($('.common-rich-editor').val() + data.content);
            },
            error: function() {
                alert('挿入に失敗しました');
            }
        });
    });
});