$(function () {
    var $imgField = $('.img_field');

    $imgField.on('click', function () {
        $(this).parent().find('.image_file').click();
    });

    var $fileField = $('.image_file');

    // 選択された画像を取得し表示
    $($fileField).on('change', function (e) {
        var file = e.target.files[0];

        if (file.type.indexOf('png') == -1 && file.type.indexOf('jpg') == -1 && file.type.indexOf('jpeg') == -1 && file.type.indexOf('ico') == -1 && file.type.indexOf('svg') == -1) {
            $(this).val('');
            alert('ファイル拡張子は「png/jpg/jpeg/ico/svg」のみ指定できます');
            return;
        }

        var reader = new FileReader();
        var $preview = $(this).parent().find('.img_field');

        reader.onload = (function (file) {
            return function (e) {
                $preview.empty();
                $preview.append($('<img>').attr({
                    src: e.target.result,
                    width: "100%",
                    class: "preview",
                    title: file.name
                }));
            };
        })(file);
        reader.readAsDataURL(file);
    });
});