/* Summernote とその言語プラグインのインポート */
//= require summernote

$(function () {
    if ($('.common-rich-editor').length == 0) {
        return;
    }
    $('.common-rich-editor').summernote({
        lang: 'ja-JP',
        height: 600,
        fontNames: ['Helvetica', 'sans-serif', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
        fontNamesIgnoreCheck: ['Helvetica', 'sans-serif', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            // ['insert', ['link', 'picture', 'video']],
            ['insert', ['link', 'picture', 'videoUpload']],
            ['table', ['table']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'image3Resize', 'resizeNone']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty:false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            onVideoUpload: function(files) {
                sendVideoFile(files[0], $(this));
            },
            onInit: function () {
                $(this).summernote('code', $(this).val());
            }
        }
    });
    // $('.common-rich-editor').summernote('pasteHTML', $('.common-rich-editor').val());
    $('#rich-editor-preview-button').on('click', function () {
        $('#rich-editor-preview').append($('.common-rich-editor').summernote('code'));
        $('#rich-editor-preview-button').hide();
        $('#rich-editor-edit-button').show();
        $('.note-editor').hide();
    });
    $('#rich-editor-edit-button').on('click', function () {
        $('#rich-editor-preview').children().remove();
        $('#rich-editor-preview-button').show();
        $('#rich-editor-edit-button').hide();
        $('.note-editor').show();
    });

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('upload_image', file);
        var serviceIdentifier = $('#js-insert-post-template').data('service_identifier');
        $.ajax({
            url: '/services/' + serviceIdentifier + '/posts/upload_image',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data) {
                $('.common-rich-editor').summernote('insertImage', data.url);
            },
            error: function() {
                alert('画像のアップロードに失敗しました');
            }
        });
    }

    function sendVideoFile(file, $editor) {
        // data = new FormData();
        // data.append('upload_image', file);
        // var serviceIdentifier = $('#js-insert-post-template').data('service_identifier');
        // $.ajax({
        //     url: '/services/' + serviceIdentifier + '/posts/upload_image',
        //     data: data,
        //     cache: false,
        //     contentType: false,
        //     processData: false,
        //     type: 'POST',
        //     success: function(data) {
        //         $('.common-rich-editor').summernote('insertImage', data.url);
        //     },
        //     error: function() {
        //         alert('画像のアップロードに失敗しました');
        //     }
        // });

        $editor.summernote('videoUpload.insertVideoLinkOrCallback', 'https://matchingbase.s3-ap-northeast-1.amazonaws.com/develop_post_content_image/mov_hts-samp007.mp4');



    //     $.ajax({
    //         type: 'POST',
    //         url: 'xxxxx',
    //         data: {
    //             fileSize: file.size,
    //             fileName: file.name,
    //             fileType: file.type
    //         },beforeSend(){
    //             $editor.summernote('videoUpload.showVideoUploadMessage', '動画をアップロードしています。');
    //         }}).then(function(data) {
    //             $editor.summernote('videoUpload.insertVideoLinkOrCallback', data.videoUri);
    //         })
    //         .catch((...args) => {
    //         console.log('error');
    //     })
    // .then(function() {
    //         console.log('finish');
    //     });
    }
});