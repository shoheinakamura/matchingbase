function ChatUtil() {
    this.scrollBottom = function () {
        var $directChatMessages = $('.chat-messages');
        if ($directChatMessages.length > 0) {
            $directChatMessages[0].scrollTop = $directChatMessages[0].scrollHeight;
        }
    };
}