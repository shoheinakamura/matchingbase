$(function() {
    var chatUtil = new ChatUtil();

    App.chat = App.cable.subscriptions.create({ channel: "ChatChannel", chat_room_identifier: $('#chat_messages').data('chat_room_identifier') }, {
        connected: function() {
        },
        received: function(data) {
            if (data['user']['id'] == $('#chat_messages').data('user_id')) {
                $('.chat-messages').append(data['own_message_html']);
            } else {
                $('.chat-messages').append(data['other_message_html']);
            }
            // 別のauthenticity_tokenが設定されているため書き換える
            var token = $('meta[name="csrf-token"]').attr('content');
            $('.chat-messages .direct-chat-msg [name="authenticity_token"]').val(token);
            chatUtil.scrollBottom();
            // last_read_atの更新
            updateLastReadAt($('#chat_messages').data('user_id'), $('#chat_messages').data('chat_room_identifier'));
        },
        speak: function (content) {
            return this.perform('speak', {
                content: content,
                chat_room_identifier: $('#chat_messages').data('chat_room_identifier')
            });
        }
    });

    function updateLastReadAt(user_id, chat_room_identifier) {
        data = new FormData();
        data.append('user_id', user_id);
        data.append('chat_room_identifier', chat_room_identifier);
        $.ajax({
            url: '/services/' + $('.direct-chat').data('service_identifier') + '/chat_room/update_last_read_at',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data) {
            },
            error: function() {
            }
        });
    }
});