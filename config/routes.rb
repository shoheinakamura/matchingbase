Rails.application.routes.draw do
  devise_for :user, skip: :all
  devise_scope :user do
    get 'services/:service_identifier/users/cancel', to: 'services/users/registrations#cancel', as: :cancel_user_registration
    get 'services/:service_identifier/users/sign_up', to: 'services/users/registrations#new', as: :new_user_registration
    get 'services/:service_identifier/users/edit', to: 'services/users/registrations#edit', as: :edit_user_registration
    patch 'services/:service_identifier/users', to: 'services/users/registrations#update', as: :user_registration
    put 'services/:service_identifier/users', to: 'services/users/registrations#update'
    delete 'services/:service_identifier/users', to: 'services/users/registrations#destroy'
    post 'services/:service_identifier/users', to: 'services/users/registrations#create'

    get 'services/:service_identifier/users/confirmation/new', to: 'services/users/confirmations#new', as: :new_user_confirmation
    get 'services/:service_identifier/users/confirmation', to: 'services/users/confirmations#show'
    post 'services/:service_identifier/users/confirmation', to: 'services/users/confirmations#create', as: :user_confirmation

    get 'services/:service_identifier/users/sign_in', to: 'services/users/sessions#new', as: :new_user_session
    post 'services/:service_identifier/users/sign_in', to: 'services/users/sessions#create', as: :user_session
    delete 'services/:service_identifier/users/sign_out', to: 'services/users/sessions#destroy', as: :destroy_user_session

    get 'services/:service_identifier/users/password/new', to: 'services/users/passwords#new', as: :new_user_password
    get 'services/:service_identifier/users/password/edit', to: 'services/users/passwords#edit', as: :edit_user_password
    patch 'services/:service_identifier/users/password', to: 'services/users/passwords#update', as: :user_password
    put 'services/:service_identifier/users/password', to: 'services/users/passwords#update'
    post 'services/:service_identifier/users/password', to: 'services/users/passwords#create'
  end
  devise_for :providers, controllers: {
    sessions:      'providers/sessions',
    passwords:     'providers/passwords',
    registrations: 'providers/registrations',
    confirmations: 'providers/confirmations'
  }
  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?

  root 'tops#index'

  resource :service_lp, only: %i[show]
  resource :commercial_transaction, only: %i[show]
  resource :service_term, only: %i[show]
  resource :privacy_policy, only: %i[show]
  resource :contact, only: %i[show create]

  namespace :admin do
    get 'dashboard', to: 'dashboard#index'
  end

  resource :tops, only: %i[index] do
    collection { post :pay }
    collection { post :register_credit_card }
    collection { post :change_credit_card }
    collection { post :continue_register_credit_card }
    collection { post :cancel_register_credit_card }
  end

  namespace :providers, only: [] do
    resource :profile, only: %i[show update]
    resource :unsubscribe, only: %i[create]
  end

  resources :manage_services, param: :identifier
  resources :manage_service_matchings, param: :identifier, only: [:edit, :update]
  resources :manage_service_prices, param: :identifier, only: [:edit, :update]
  resources :manage_service_admins, param: :identifier, only: [:edit, :update]
  resources :manage_service_lps, param: :identifier, only: [:edit, :update]
  resources :manage_service_uis, param: :identifier, only: [:edit, :update]
  resources :manage_service_releases, param: :identifier, only: [:edit, :update]
  resources :manage_payments, param: :identifier, only: [:index, :update]
  resources :manage_users, param: :identifier, only: [:index, :show, :destroy] do
    member { get :chat_room }
  end
  resources :manage_posts, param: :identifier, only: [:index, :edit, :update, :destroy]

  # resources :services, only: %i[show]

  resources :services, only: %i[show], param: :identifier, module: :services do

    resource :users, only: [], module: :users do
      resource :mypage, only: %i[show update]
      resource :bank, only: %i[show update]
      resource :plan, only: %i[show]
      resource :unsubscribe, only: %i[create]
      resource :payment, only: [:show] do
        collection { post :pay }
        collection { post :register_credit_card }
        collection { post :change_credit_card }
        collection { post :continue_register_credit_card }
        collection { post :cancel_register_credit_card }
        collection { post :register_plan }
        collection { post :cancel_plan }
      end
    end

    resources :posts, only: %i[index show new create destroy edit update], param: :identifier do
      collection {post :upload_image}
      collection {get :search}
      collection {post :insert_post_template}
      member {patch :change_status}
    end
    resources :chat_room, only: %i[create index show], param: :identifier do
      collection {post :upload_file}
      collection {post :download_file}
      collection {post :update_last_read_at}
    end
    resources :post_entries, only: %i[create index show update], param: :identifier
    resource :own_posts, only: %i[show]

    resource :stop_alert, only: %i[show]

    resource :service_admin, only: %i[show]
    resource :service_lp, only: %i[show]
    resource :commercial_transaction, only: %i[show]
    resource :service_term, only: %i[show]
    resource :privacy_policy, only: %i[show]
    resource :contact, only: %i[show create]
  end
end
