CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  config.permissions = 0666
  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: Rails.application.credentials.dig(:aws_s3, :access_key_id),
    aws_secret_access_key: Rails.application.credentials.dig(:aws_s3, :secret_access_key),
    region: 'ap-northeast-1'
  }
  config.fog_directory  = 'matchingbase' # S3のバケット名
  config.cache_storage = :fog
end