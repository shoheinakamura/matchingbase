set :user, 'ec2-user'
set :stage, :production
set :rails_env, 'production'
set :branch, ENV['BRANCH'] || 'master'
set :migration_role, 'db'
set :log_level, :debug
set :pty, false
set :ssh_options, {
  user: 'ec2-user',
  keys: %w{~/.ssh/matchingbase-key.pem},
  forward_agent: true,
  auth_methods: %w{publickey}
}

# production-a
role :app, %w[ec2-user@18.176.24.151]
role :web, %w[ec2-user@18.176.24.151]
role :db, %w[ec2-user@18.176.24.151]